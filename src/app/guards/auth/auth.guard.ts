import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AbstractLoggerService} from '../../features/logging/services/log/abstract-logger.service';
import {AuthenticationService} from '../../features/user/services/auth/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class AuthGuard implements CanActivate {


    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private logger: AbstractLoggerService
    ) {
    }

    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {

        if (!this.authenticationService.isLoggedIn()) {
            this.router.navigate(['/login'], {queryParams: {returnUrl: state.url}}).then(() => {
                this.logger.logInfo('The user is not authenticated, redirecting to login.');
            });
        }
        // TODO: attempt refresh
        return this.authenticationService.isLoggedIn();
    }
}
