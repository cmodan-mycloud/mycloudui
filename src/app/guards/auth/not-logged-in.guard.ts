import {Injectable} from '@angular/core';
import {ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AbstractAuthenticationService} from '../../features/user/services/auth/abstract-authentication.service';

@Injectable({
    providedIn: 'root'
})
export class NotLoggedIn implements CanActivate {

    constructor(
        private authenticationService: AbstractAuthenticationService,
        private router: Router
    ) {
    }

    async canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot) {
        const loggedIn = this.authenticationService.isLoggedIn();
        if (loggedIn) {
            await this.router.navigate(['/home']);
        }

        return !loggedIn;
    }

}
