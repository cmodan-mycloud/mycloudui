import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractIdService {

    abstract generate(): string;
}
