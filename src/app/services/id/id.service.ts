import {Injectable} from '@angular/core';
import {AbstractIdService} from './abstract.id.service';
import {UUID} from 'angular2-uuid';

@Injectable({
    providedIn: 'root'
})
export class IdService implements AbstractIdService {

    constructor() {
    }

    generate(): string {
        return UUID.UUID().toString();
    }
}
