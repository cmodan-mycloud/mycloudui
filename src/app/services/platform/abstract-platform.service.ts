import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractPlatformService {
    abstract isMobile(): boolean;

    abstract isBrowserBased(): boolean;
}
