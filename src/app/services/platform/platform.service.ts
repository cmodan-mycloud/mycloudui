import {Injectable} from '@angular/core';
import {AbstractPlatformService} from './abstract-platform.service';
import {Platform} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class PlatformService implements AbstractPlatformService {
    private platforms: string[];
    private mobilePlatforms = [
        'ios',
        'android'
    ];

    constructor(
        private platform: Platform,
    ) {
        this.platform.ready().then(() => {
            this.platforms = this.platform.platforms();
        });
    }

    isBrowserBased(): boolean {
        return this.platforms.some(r => this.mobilePlatforms.indexOf(r) === -1);
    }

    isMobile(): boolean {
        return this.platforms.some(r => this.mobilePlatforms.indexOf(r) >= 0);
    }
}
