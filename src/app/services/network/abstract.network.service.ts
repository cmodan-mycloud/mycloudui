import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConnectionStatus } from './network.service';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractNetworkService {
    public abstract onNetworkChange(): Observable<ConnectionStatus>;
    public abstract getCurrentNetworkStatus(): ConnectionStatus;
    public abstract setConnectionSubscriptions(): void;
}
