import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { Platform, AlertController } from '@ionic/angular';
import { Network } from '@ionic-native/network/ngx';
import { AbstractNetworkService } from './abstract.network.service';
import {AbstractLoggerService} from '../../features/logging/services/log/abstract-logger.service';


export enum ConnectionStatus {
    Online,
    Offline
}

@Injectable({
    providedIn: 'root'
})
export class NetworkService implements AbstractNetworkService {
    private status: BehaviorSubject<ConnectionStatus> = new BehaviorSubject<ConnectionStatus>(ConnectionStatus.Offline);

    constructor(
        private network: Network,
        private platform: Platform,
        private alertController: AlertController,
        private logger: AbstractLoggerService
    ) { }

    public setConnectionSubscriptions(): void {
        this.platform.ready().then(() => {
            this.initializeNetworkEvents();
            const status = this.network.type !== 'none' ? ConnectionStatus.Online : ConnectionStatus.Offline;
            this.status.next(status);
        });
    }

    private initializeNetworkEvents(): void {
        this.network.onDisconnect().subscribe(
            async () => {
                if (this.status.getValue() === ConnectionStatus.Online) {
                    this.status.next(ConnectionStatus.Offline);
                    this.logger.logInfo(`We are now offline!`);
                    this.alertController.create({
                        header: 'Network status',
                        animated: true,
                        message: 'You are now offline! Please check your connection!',
                        buttons: [],
                        translucent: false,
                        backdropDismiss: false
                    }).then(async (alert: HTMLIonAlertElement) => {
                        await alert.present();
                    });
                }
            }
        );

        this.network.onConnect().subscribe(
            async () => {
                if (this.status.getValue() === ConnectionStatus.Offline) {
                    this.status.next(ConnectionStatus.Offline);
                    this.logger.logInfo(`We are now online!`);
                    await this.alertController.dismiss();
                }
            });
    }

    public onNetworkChange(): Observable<ConnectionStatus> {
        return this.status.asObservable();
    }

    public getCurrentNetworkStatus(): ConnectionStatus {
        return this.status.getValue();
    }
}
