import { Observable } from 'rxjs';
import { ShareItem } from '../../models/share.model';

export abstract class AbstractSharingService {
    abstract shareItems(items: ShareItem, userEmail: string): Observable<boolean>;
    abstract shareFolder(folderId: string, userEmail: string): Observable<boolean>;
    abstract shareFile(fileId: string, userEmail: string);
    abstract incomingShares(): Observable<ShareItem[]>;
    abstract outgoingShares(): Observable<ShareItem[]>;
}
