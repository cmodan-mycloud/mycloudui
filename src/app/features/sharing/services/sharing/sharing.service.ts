import {AbstractSharingService} from './abstract.sharing.service';
import {EMPTY, Observable} from 'rxjs';
import {ShareItem} from '../../models/share.model';
import {HttpClient} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractAlertService} from '../../../drawables/services/alert/abstract.alert.service';

@Injectable({
    providedIn: 'root'
})
export class SharingService implements AbstractSharingService {
    constructor(
        private httpClient: HttpClient,
        private logger: AbstractLoggerService,
        private alertService: AbstractAlertService
    ) {
    }

    outgoingShares(): Observable<ShareItem[]> {
        return EMPTY;
    }

    shareFolder(folderId: string): Observable<boolean> {
        return EMPTY;
    }

    shareFile(fileId: string, userEmail: string) {
        return EMPTY;
    }

    incomingShares(): Observable<ShareItem[]> {
        return EMPTY;
    }

    shareItems(items: ShareItem, userEmail: string): Observable<boolean> {
        return EMPTY;
    }

}
