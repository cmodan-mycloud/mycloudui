import {File} from '../../items/models/file.model';
import {Folder} from '../../items/models/folder.model';

export interface ShareItem {
    ownerId: string;
    files: File[];
    folders: Folder[];
}
