import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbstractSharingService} from './services/sharing/abstract.sharing.service';
import {SharingService} from './services/sharing/sharing.service';
import {SharesListComponent} from './components/shares-list/shares-list.component';
import {IonicModule} from '@ionic/angular';
import {AbstractShareStoreService} from './store/shares/abstract-share-store.service';
import {ShareStoreService} from './store/shares/share-store.service';
import {DrawablesModule} from '../drawables/drawables.module';
import {LoggingModule} from '../logging/logging.module';


@NgModule({
    declarations: [
        SharesListComponent
    ],
    providers: [
        {provide: AbstractSharingService, useClass: SharingService},
        {provide: AbstractShareStoreService, useClass: ShareStoreService},
    ],
    imports: [
        CommonModule,
        IonicModule,
        DrawablesModule,
        LoggingModule,
    ],
    exports: [
        SharesListComponent,
    ]
})
export class SharingModule {
}
