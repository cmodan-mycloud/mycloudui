import {Injectable} from '@angular/core';
import {AbstractShareStoreService} from './abstract-share-store.service';
import {ShareItem} from '../../models/share.model';
import {Observable} from 'rxjs';
import {AbstractSharingService} from '../../services/sharing/abstract.sharing.service';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';

@Injectable({
    providedIn: 'root'
})
export class ShareStoreService implements AbstractShareStoreService {
    public incomingShares: Observable<ShareItem[]>;
    public outgoingShares: Observable<ShareItem[]>;


    constructor(
        private sharingService: AbstractSharingService,
        private logger: AbstractLoggerService
    ) {
    }


    getIncomingShares(): void {
    }

    getOutgoingShares(): void {
    }

    revokeShare(id: string): void {
    }

    shareFile(id: string): void {
    }

    shareFolder(id: string): void {
    }
}
