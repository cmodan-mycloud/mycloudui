import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {ShareItem} from '../../models/share.model';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractShareStoreService {
    public incomingShares: Observable<ShareItem[]>;
    public outgoingShares: Observable<ShareItem[]>;

    abstract getIncomingShares(): void;

    abstract getOutgoingShares(): void;

    abstract revokeShare(id: string): void;

    abstract shareFolder(id: string): void;

    abstract shareFile(id: string): void;
}
