import {Component, Input, OnInit} from '@angular/core';
import {ShareItem} from '../../models/share.model';

@Component({
    selector: 'app-shares-list',
    templateUrl: './shares-list.component.html',
    styleUrls: ['./shares-list.component.scss'],
})
export class SharesListComponent implements OnInit {
    @Input() incomingShares: ShareItem[];
    @Input() outgoingShares: ShareItem[];

    constructor() {
    }

    ngOnInit() {
    }

}
