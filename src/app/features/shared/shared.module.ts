import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HoveredDirective} from './directives/hoverable/hovered.directive';
import {ContextDirective} from './directives/context/context.directive';


@NgModule({
    declarations: [
        HoveredDirective,
        ContextDirective,
    ],
    entryComponents: [],
    imports: [
        CommonModule
    ],
    exports: [
        HoveredDirective,
        ContextDirective
    ]
})
export class SharedModule {
}
