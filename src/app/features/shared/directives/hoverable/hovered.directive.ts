import {Directive, EventEmitter, HostListener, Output} from '@angular/core';

@Directive({
    selector: '[appHovered]'
})
export class HoveredDirective {
    @Output() mouseOver: EventEmitter<void> = new EventEmitter<void>();
    @Output() mouseLeave: EventEmitter<void> = new EventEmitter<void>();

    constructor() {
    }

    @HostListener('mouseover', [])
    onHover() {
        this.mouseOver.emit();
    }

    @HostListener('mouseleave', [])
    onClick() {
        this.mouseLeave.emit();
    }
}
