import {Directive, HostListener} from '@angular/core';

@Directive({
  selector: '[appContext]'
})
export class ContextDirective {

  @HostListener('contextmenu', ['$event'])
  onRightClick(event) {
    event.preventDefault();
  }

}
