import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {Connect, Disconnect, InitializeConnection} from './hub.actions';
import {AbstractRealtimeService} from '../services/abstract-realtime.service';

export interface HubStateDefinition {
    connected: boolean;
}


@State<HubStateDefinition>({
    name: 'hub',
    defaults: {
        connected: false
    }
})
@Injectable()
export class HubState {

    constructor(
        private hubService: AbstractRealtimeService
    ) {
    }

    @Action(InitializeConnection)
    initializeConnection(ctx: StateContext<HubStateDefinition>) {
        const result = this.hubService.connect();
        ctx.patchState({connected: result});
    }

    @Action(Connect)
    connect(ctx: StateContext<HubStateDefinition>) {
        const result = this.hubService.connect();
        ctx.patchState({connected: result});
    }

    @Action(Disconnect)
    disconnect(ctx: StateContext<HubStateDefinition>) {
        this.hubService.disconnect();
        ctx.patchState({connected: false});
    }
}
