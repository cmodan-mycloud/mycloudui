export class InitializeConnection {
    static readonly type = '[HUB] Initialize connection';
}

export class Connect {
    static readonly type = '[HUB] Connect';
}

export class Disconnect {
    static readonly type = '[HUB] Disconnect';
}
