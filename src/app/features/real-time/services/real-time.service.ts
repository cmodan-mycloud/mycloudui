import {Injectable} from '@angular/core';
import {AbstractRealtimeService} from './abstract-realtime.service';
import {environment} from '../../../../environments/environment';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import {BehaviorSubject} from 'rxjs';
import {ServerNotification} from './models/server-notification';
import {File} from '../../items/models/file.model';
import {AbstractLoggerService} from '../../logging/services/log/abstract-logger.service';
import {AbstractAuthenticationService} from '../../user/services/auth/abstract-authentication.service';

@Injectable({
    providedIn: 'root'
})
export class RealTimeService implements AbstractRealtimeService {
    private connection: HubConnection;
    private dataSubject = new BehaviorSubject<File>(undefined);

    public data$ = this.dataSubject.asObservable();

    constructor(
        private logger: AbstractLoggerService,
        private authenticationService: AbstractAuthenticationService
    ) {
    }


    private createConnection() {
        const token = this.authenticationService.getAccessToken();
        this.connection = new HubConnectionBuilder()
            .withUrl(`${environment.signalR.endpoint}/files`, {
                    accessTokenFactory(this): string | Promise<string> {
                        return token;
                    },
                    logMessageContent: false
                },
            )
            .build();
    }

    private startConnection(): void {
        this.connection
            .start()
            .then(() => {
                this.logger.logDebug('Connection successfully established to the signalRHub');
            });
    }

    private addListeners() {
        this.connection.on('fileUploaded', (response: any) => {
            const data: ServerNotification = response as ServerNotification;
            this.logger.logDebug(`New file entry from server: ${data.payload.name}.`);
            this.dataSubject.next(data.payload);
        });
    }

    connect(): boolean {
        try {
            this.logger.logDebug('Attempting to connect to the hub...');
            this.createConnection();
            this.startConnection();
            this.addListeners();
        } catch (e) {
            this.logger.logError(`Encountered fault when trying to establish hub connection: ${e.message}`);
            return false;
        }
        return true;
    }

    disconnect(): void {
        this.logger.logDebug('Attempting to stop to the hub connection.');
        this.connection.stop().then(() => {
            this.logger.logDebug(`Successfully stopped hub connection.`);
        });
    }
}
