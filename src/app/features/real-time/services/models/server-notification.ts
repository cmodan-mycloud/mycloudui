import {File} from '../../../items/models/file.model';

export interface ServerNotification {
    payload: File;
}
