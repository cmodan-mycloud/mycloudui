import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {File} from '../../items/models/file.model';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractRealtimeService {
    public data$: Observable<File>;

    abstract connect(): boolean;

    abstract disconnect(): void;
}
