import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbstractRealtimeService} from './services/abstract-realtime.service';
import {RealTimeService} from './services/real-time.service';
import {DrawablesModule} from '../drawables/drawables.module';
import {LoggingModule} from '../logging/logging.module';
import {NgxsModule} from '@ngxs/store';
import {HubState} from './state/hub.state';
import {NotificationsModule} from '../notifications/notifications.module';
import {UserModule} from '../user/user.module';


@NgModule({
    declarations: [],
    providers: [
        {provide: AbstractRealtimeService, useClass: RealTimeService}
    ],
    entryComponents: [],
    imports: [
        CommonModule,
        DrawablesModule,
        LoggingModule,
        NgxsModule.forFeature([HubState]),
        NotificationsModule,
        UserModule
    ],
    exports: []
})
export class RealTimeModule {
}
