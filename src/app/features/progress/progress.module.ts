import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbstractProgressService} from './services/progress/abstract-progress.service';
import {ProgressService} from './services/progress/progress.service';


@NgModule({
    declarations: [],
    providers: [
        {provide: AbstractProgressService, useClass: ProgressService}
    ],
    imports: [
        CommonModule
    ]
})
export class ProgressModule {
}
