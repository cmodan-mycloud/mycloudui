import {Observable} from 'rxjs';
import {HttpEvent} from '@angular/common/http';

export interface TransactionModel {
    eventSource: Observable<HttpEvent<any>>;
    tag: string;
}
