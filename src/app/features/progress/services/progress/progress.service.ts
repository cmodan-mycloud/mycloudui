import {Injectable} from '@angular/core';
import {AbstractProgressService} from './abstract-progress.service';
import {BehaviorSubject, Observable} from 'rxjs';
import {TransactionModel} from './models/transaction-model';

@Injectable({
    providedIn: 'root'
})
export class ProgressService implements AbstractProgressService {
    private transactions: TransactionModel[] = [];
    private subject: BehaviorSubject<TransactionModel[]> = new BehaviorSubject(this.transactions);

    public transactions$: Observable<TransactionModel[]> = this.subject.asObservable();

    publish(transaction: TransactionModel): void {
        this.transactions.push(transaction);
        this.subject.next(this.transactions);
    }

    complete(transaction: TransactionModel): void {
        this.transactions = this.transactions.filter(t => t !== transaction);
        this.subject.next(this.transactions);
    }
}
