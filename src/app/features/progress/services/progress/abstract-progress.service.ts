import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {TransactionModel} from './models/transaction-model';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractProgressService {
    public transactions$: Observable<TransactionModel[]>;

    abstract publish(transaction: TransactionModel): void;
    abstract complete(transaction: TransactionModel): void;
}
