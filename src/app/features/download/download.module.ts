import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LoggingModule} from '../logging/logging.module';
import {AbstractDownloadService} from './services/download/abstract.download.service';
import {DownloadService} from './services/download/download.service';
import {ProgressModule} from '../progress/progress.module';
import {DownloadManagerService} from './services/manager/download-manager.service';
import {AbstractDownloadManagerService} from './services/manager/abstract-download-manager.service';
import {NotificationsModule} from '../notifications/notifications.module';
import {FileSaverModule} from 'ngx-filesaver';


@NgModule({
    declarations: [],
    providers: [
        {provide: AbstractDownloadService, useClass: DownloadService},
        {provide: AbstractDownloadManagerService, useClass: DownloadManagerService},
    ],
    imports: [
        LoggingModule,
        CommonModule,
        ProgressModule,
        NotificationsModule,
        FileSaverModule
    ]
})
export class DownloadModule {
}
