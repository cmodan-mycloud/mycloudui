import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import {HttpEvent} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractDownloadService {
  abstract downloadFile(fileId: string): Observable<HttpEvent<any>>;
  abstract downloadFolder(folderId: string): Observable<HttpEvent<any>>;
}
