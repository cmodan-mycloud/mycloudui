import {Injectable} from '@angular/core';
import {AbstractDownloadService} from './abstract.download.service';
import {EMPTY, Observable} from 'rxjs';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {shareReplay} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class DownloadService implements AbstractDownloadService {

    constructor(
        private httpClient: HttpClient
    ) {
    }

    downloadFolder(folderId: any): Observable<HttpEvent<any>> {
        return EMPTY;
    }

    downloadFile(fileId: string): Observable<HttpEvent<any>> {
        return this.httpClient.get(`${environment.downloadUrl}/file`, {
            params: {
                FileId: fileId
            },
            responseType: 'blob',
            observe: 'events'
        }).pipe(shareReplay(1));
    }
}
