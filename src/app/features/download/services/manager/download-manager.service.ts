import {Injectable} from '@angular/core';
import {AbstractDownloadManagerService} from './abstract-download-manager.service';
import {AbstractDownloadService} from '../download/abstract.download.service';
import {AbstractProgressService} from '../../../progress/services/progress/abstract-progress.service';
import {FileDownloadModel} from './models/file-download-model';
import {FolderDownloadModel} from './models/folder-download-model';
import {TransactionModel} from '../../../progress/services/progress/models/transaction-model';
import {filter, map, take} from 'rxjs/operators';
import {HttpEventType, HttpResponse} from '@angular/common/http';
import {AbstractNotificationService} from '../../../notifications/services/notification/abstract-notification.service';
import {Observable} from 'rxjs';
import {FileSaverService} from 'ngx-filesaver';

@Injectable({
    providedIn: 'root'
})
export class DownloadManagerService implements AbstractDownloadManagerService {

    constructor(
        private downloadService: AbstractDownloadService,
        private progressService: AbstractProgressService,
        private notificationsService: AbstractNotificationService,
        private fileSaver: FileSaverService
    ) {
    }

    downloadFileLocally(request: FileDownloadModel): void {
        const request$ = this.downloadService.downloadFile(request.fileId);
        const transaction: TransactionModel = {eventSource: request$, tag: `Downloading ${request.fullName}.`};
        this.progressService.publish(transaction);
        this.beginLocalDownloadTransaction(transaction, request.fullName);
    }

    downloadFolder(request: FolderDownloadModel): Blob {
        return undefined;
    }

    downloadPhotoAsString(fileId: string): Observable<string> {
        return this.downloadService.downloadFile(fileId)
            .pipe(
                filter(events => events.type === HttpEventType.Response),
                map((response: HttpResponse<Blob>) => {
                    return URL.createObjectURL(response.body);
                })
            );
    }

    private beginLocalDownloadTransaction(transaction: TransactionModel, fullName: string) {
        transaction.eventSource
            .pipe(
                filter(events => events.type === HttpEventType.Response),
                take(1)
            ).subscribe(
            (response: HttpResponse<Blob>) => {
                this.fileSaver.save(response.body, fullName);
            },
            error => {
                this.notificationsService.fireFailureNotification(error.mesage, null);
            },
            () => {
                this.progressService.complete(transaction);
            }
        );
    }
}
