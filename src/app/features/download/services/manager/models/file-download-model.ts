export interface FileDownloadModel {
    fileId: string;
    fullName: string;
}
