export interface FolderDownloadModel {
    folderId: string;
    name: string;
}
