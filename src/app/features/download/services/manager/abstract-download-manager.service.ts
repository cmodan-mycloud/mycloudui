import {Injectable} from '@angular/core';
import {FileDownloadModel} from './models/file-download-model';
import {FolderDownloadModel} from './models/folder-download-model';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractDownloadManagerService {
    abstract downloadFileLocally(request: FileDownloadModel): void;

    abstract downloadFolder(request: FolderDownloadModel): Blob;

    abstract downloadPhotoAsString(fileId: string): Observable<string>;

}
