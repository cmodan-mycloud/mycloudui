import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {File} from '../../../items/models/file.model';
import {ModalController, PopoverController} from '@ionic/angular';
import {Store} from '@ngxs/store';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {AbstractNotificationService} from '../../../notifications/services/notification/abstract-notification.service';
import {DeletePhoto} from '../../store/photos.actions';

@Component({
    selector: 'app-photo-modal',
    templateUrl: './photo-modal.component.html',
    styleUrls: ['./photo-modal.component.scss'],
})
export class PhotoModalComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    @Input() photo: File;
    @Input() id: string;
    @Input() data: string;

    constructor(
        private modalController: ModalController,
        private popoverController: PopoverController,
        private store: Store,
        private idService: AbstractIdService,
        private notificationsService: AbstractNotificationService
    ) {
    }

    ngOnInit() {
    }


    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async dismiss() {
        await this.modalController.dismiss(null, null, this.id);
    }

    delete(): void {
        this.modalController.dismiss(null, null, this.id).then(() => {
            this.store.dispatch(new DeletePhoto(this.photo.id))
                .subscribe(
                    () => {
                        const message = `Successfully delete photo ${this.photo.name}`;
                        this.notificationsService.fireSuccessfulNotification(message, 'Success');
                    },
                    error => this.notificationsService.fireSuccessfulNotification(error.message, 'Error')
                );
        });
    }
}
