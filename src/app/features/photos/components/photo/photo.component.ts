import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {File} from '../../../items/models/file.model';
import {ModalController} from '@ionic/angular';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {PhotoModalComponent} from '../photo-modal/photo-modal.component';
import {AbstractDownloadManagerService} from '../../../download/services/manager/abstract-download-manager.service';

@Component({
    selector: 'app-photo',
    templateUrl: './photo.component.html',
    styleUrls: ['./photo.component.scss'],
})
export class PhotoComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    @Input() photo: File;
    public image: string;

    constructor(
        private downloadManagerService: AbstractDownloadManagerService,
        private modalController: ModalController,
        private idService: AbstractIdService
    ) {
    }

    ngOnInit() {
        this.downloadManagerService
            .downloadPhotoAsString(this.photo.id)
            .pipe(
                takeUntil(this.destroyed$),
            )
            .subscribe((data: string) => {
                    this.image = data;
                }
            );
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async show(): Promise<void> {
        const popoverId = this.idService.generate();
        const popover = await this.modalController.create({
            id: popoverId,
            component: PhotoModalComponent,
            componentProps: {
                id: popoverId,
                photo: this.photo,
                data: this.image
            },
            cssClass: 'modal-fullscreen'
        });

        await popover.present();
    }
}
