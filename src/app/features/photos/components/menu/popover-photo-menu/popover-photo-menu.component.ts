import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-popover-photo-menu',
    templateUrl: './popover-photo-menu.component.html',
    styleUrls: ['./popover-photo-menu.component.scss'],
})
export class PopoverPhotoMenuComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    @Input() id: string;

    constructor(
        private popoverController: PopoverController
    ) {
    }

    ngOnInit() {
    }


    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async openCamera() {
        await this.popoverController.dismiss(null, null, this.id);
    }


    async fromDevice(files: FileList) {
        await this.popoverController.dismiss(null, null, this.id);
    }
}
