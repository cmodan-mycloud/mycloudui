import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {AbstractCameraService} from '../../../services/camera/abstract-camera.service';
import {AbstractUploadManagerService} from '../../../../upload/services/manager/abstract-upload-manager.service';

@Component({
    selector: 'app-photo-menu-button',
    templateUrl: './photo-menu-button.component.html',
    styleUrls: ['./photo-menu-button.component.scss'],
})
export class PhotoMenuButtonComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();

    constructor(
        private cameraService: AbstractCameraService,
        private uploadManagerService: AbstractUploadManagerService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async captureAndUpload(): Promise<void> {
        try {
            const image = await this.cameraService.capture();
            this.uploadManagerService.uploadToRoot({photo: image});
        } catch (e) {
        }
    }
}
