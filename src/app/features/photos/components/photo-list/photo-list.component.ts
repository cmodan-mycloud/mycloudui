import {Component, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {Select} from '@ngxs/store';
import {File} from '../../../items/models/file.model';

@Component({
    selector: 'app-photo-list',
    templateUrl: './photo-list.component.html',
    styleUrls: ['./photo-list.component.scss'],
})
export class PhotoListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();

    @Select(state => state.photos.photos) photos$: Observable<File[]>;

    constructor() {
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
