import {Injectable} from '@angular/core';
import {AbstractCameraService} from './abstract-camera.service';
import {CameraResultType, Plugins} from '@capacitor/core';

const {Camera} = Plugins;

@Injectable({
    providedIn: 'root'
})
export class CameraService implements AbstractCameraService {
    constructor() {
    }


    async capture(): Promise<Blob> {
        const photo = await Camera.getPhoto({
            quality: 100,
            allowEditing: false,
            resultType: CameraResultType.DataUrl,
        });

        return await fetch(photo.dataUrl).then(r => r.blob());
    }
}
