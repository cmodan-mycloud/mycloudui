import {File} from '../../../../../items/models/file.model';

export interface Photos {
    photos: File[];
}
