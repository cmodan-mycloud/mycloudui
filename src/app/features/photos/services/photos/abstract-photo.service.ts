import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {Photos} from './models/response/photos';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractPhotoService {
    abstract photos(): Observable<Photos>;
}
