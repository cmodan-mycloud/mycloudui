import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {Photos} from './models/response/photos';
import {AbstractPhotoService} from './abstract-photo.service';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {environment} from '../../../../../environments/environment';
import {catchError, shareReplay} from 'rxjs/operators';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractLoadingService} from '../../../drawables/services/loading/abstract.loading.service';
import {AbstractAlertService} from '../../../drawables/services/alert/abstract.alert.service';

@Injectable({
    providedIn: 'root'
})
export class PhotoService implements AbstractPhotoService {
    constructor(
        private httpClient: HttpClient,
        private logger: AbstractLoggerService,
        private alertService: AbstractAlertService,
        private loadingService: AbstractLoadingService
    ) {
    }

    photos(): Observable<Photos> {
        const request$ = this.httpClient
            .get<Photos>(`${environment.metaDataUrl}/photos`)
            .pipe(
                catchError(err => {
                    this.handleError(err);
                    return of(null);
                }),
                shareReplay(1)
            );
        this.loadingService.showLoadingUntilCompleted(request$);
        return request$;
    }

    private handleError(error: HttpErrorResponse) {
        this.logger.logHttpError(error);

        if (error.status === 400) {
            this.alertService.present(error.message);
        }
    }

}
