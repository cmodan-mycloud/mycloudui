import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PhotoComponent} from './components/photo/photo.component';
import {PhotoListComponent} from './components/photo-list/photo-list.component';
import {AbstractPhotoService} from './services/photos/abstract-photo.service';
import {PhotoService} from './services/photos/photo.service';
import {IonicModule} from '@ionic/angular';
import {PhotoMenuButtonComponent} from './components/menu/photo-menu-button/photo-menu-button.component';
import {PopoverPhotoMenuComponent} from './components/menu/popover-photo-menu/popover-photo-menu.component';
import {PhotoModalComponent} from './components/photo-modal/photo-modal.component';
import {AbstractCameraService} from './services/camera/abstract-camera.service';
import {CameraService} from './services/camera/camera.service';
import {NgxsModule} from '@ngxs/store';
import {PhotosState} from './store/photos.state.cs';
import {LoggingModule} from '../logging/logging.module';
import {UploadModule} from '../upload/upload.module';
import {DrawablesModule} from '../drawables/drawables.module';
import {NotificationsModule} from '../notifications/notifications.module';


@NgModule({
    declarations: [
        PhotoComponent,
        PhotoListComponent,
        PhotoMenuButtonComponent,
        PopoverPhotoMenuComponent,
        PhotoModalComponent
    ],
    entryComponents: [
        PhotoComponent,
        PhotoListComponent,
        PhotoMenuButtonComponent,
        PopoverPhotoMenuComponent,
        PhotoModalComponent
    ],
    providers: [
        {provide: AbstractPhotoService, useClass: PhotoService},
        {provide: AbstractCameraService, useClass: CameraService},
    ],
    imports: [
        CommonModule,
        IonicModule,
        LoggingModule,
        UploadModule,
        DrawablesModule,
        NotificationsModule,
        NgxsModule.forFeature([PhotosState])
    ],
    exports: [
        PhotoComponent,
        PhotoListComponent,
        PhotoMenuButtonComponent,
        PopoverPhotoMenuComponent,
        PhotoModalComponent
    ]
})
export class PhotosModule {
}
