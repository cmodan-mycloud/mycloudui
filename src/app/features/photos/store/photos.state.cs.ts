import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {Photos} from '../services/photos/models/response/photos';
import {filter, tap} from 'rxjs/operators';
import {AddPhoto, DeletePhoto, GetPhotos, RenamePhoto, ResetPhotos} from './photos.actions';
import {AbstractPhotoService} from '../services/photos/abstract-photo.service';
import {DeleteFile, RenameFile} from '../../items/store/items.actions';

@State<Photos>({
    name: 'photos',
    defaults: {
        photos: []
    }
})
@Injectable()
export class PhotosState {

    constructor(
        private photoService: AbstractPhotoService
    ) {
    }

    @Action(GetPhotos)
    getPhotos(ctx: StateContext<Photos>) {
        return this.photoService.photos()
            .pipe(
                filter(result => result !== null),
                tap((result: Photos) => {
                    ctx.patchState(result);
                })
            );
    }

    @Action(AddPhoto)
    addPhoto(ctx: StateContext<Photos>, action: AddPhoto) {
        const state = ctx.getState();
        state.photos.push(action.photo);
        ctx.patchState(state);
    }

    @Action([DeleteFile, DeletePhoto])
    deletePhoto(ctx: StateContext<Photos>, action: DeleteFile) {
        const state = ctx.getState();
        if (state.photos.some(p => p.id === action.fileId)) {
            state.photos = state.photos.filter(p => p.id !== action.fileId);
            ctx.patchState(state);
        }
    }


    @Action([RenameFile, RenamePhoto])
    renamePhoto(ctx: StateContext<Photos>, action: RenameFile) {
        const state = ctx.getState();
        if (state.photos.some(p => p.id === action.fileId)) {
            const photo = state.photos.find(p => p.id === action.fileId);
            const index = state.photos.indexOf(photo);
            state.photos[index].name = action.name;
            ctx.patchState(state);
        }
    }

    @Action([ResetPhotos])
    resetItems(ctx: StateContext<ResetPhotos>) {
        ctx.setState({photos: []});
    }
}
