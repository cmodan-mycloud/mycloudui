import {File} from '../../items/models/file.model';

export class GetPhotos {
    static readonly type = '[PHOTOS] Get photos';
}

export class AddPhoto {
    static readonly type = '[PHOTOS] Add photo';
    constructor(public photo: File) {
    }
}

export class DeletePhoto {
    static readonly type = '[PHOTOS] Delete photo';
    constructor(public fileId: string) {
    }
}

export class RenamePhoto {
    static readonly type = '[PHOTOS] Rename photo';
    constructor(public fileId: string, public name: string) {
    }
}

export class ResetPhotos {
    static readonly type = '[Photos] Reset';
}
