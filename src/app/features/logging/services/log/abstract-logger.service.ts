import { Injectable } from '@angular/core';
import { HttpErrorResponse } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractLoggerService {
    abstract logDebug(message: string): void;

    abstract logInfo(message: string): void;

    abstract logError(message: string): void;

    abstract logHttpError(error: HttpErrorResponse): void;
}

export enum LogLevel {
    All = 0,
    Debug = 1,
    Info = 2,
    Error = 4,
    Off = 6
}
