import {Injectable} from '@angular/core';
import {AbstractLoggerService, LogLevel} from './abstract-logger.service';
import {HttpErrorResponse} from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ConsoleLoggerService implements AbstractLoggerService {
    private level = LogLevel.Debug;

    logDebug(message: string): void {
        this.log(LogLevel.Debug, message);
    }

    logError(message: string): void {
        this.log(LogLevel.Error, message);
    }

    logInfo(message: string): void {
        this.log(LogLevel.Info, message);
    }

    logHttpError(error: HttpErrorResponse): void {
        this.log(LogLevel.Error, `Status: ${error.status}. Message: ${error.message}.`);
    }

    private log(level: LogLevel, message: string): void {
        if ((level >= this.level &&
            level !== LogLevel.Off) ||
            this.level === LogLevel.All) {
            console.log(`[Log]:${message}`);
        }
    }
}

