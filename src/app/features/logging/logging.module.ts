import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AbstractLoggerService} from './services/log/abstract-logger.service';
import {ConsoleLoggerService} from './services/log/console-logger.service';



@NgModule({
  declarations: [],
  providers: [
    {provide: AbstractLoggerService, useClass: ConsoleLoggerService}
  ],
  imports: [
    CommonModule
  ]
})
export class LoggingModule { }
