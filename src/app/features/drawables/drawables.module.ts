import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {IonicModule} from '@ionic/angular';
import {FormsModule} from '@angular/forms';
import {NameInputBoxComponent} from './components/name-input-box/name-input-box.component';
import {EmailInputBoxComponent} from './components/email-input-box/email-input-box.component';
import {LoggingModule} from '../logging/logging.module';
import {AbstractLoadingService} from './services/loading/abstract.loading.service';
import {LoadingService} from './services/loading/loading.service';
import {AbstractPopoverService} from './services/popover/abstract.popover.service';
import {PopoverService} from './services/popover/popover.service';
import {AbstractAlertService} from './services/alert/abstract.alert.service';
import {AlertService} from './services/alert/alert.service';


@NgModule({
    declarations: [
        NameInputBoxComponent,
        EmailInputBoxComponent
    ],
    providers: [
        {provide: AbstractLoadingService, useClass: LoadingService},
        {provide: AbstractPopoverService, useClass: PopoverService},
        {provide: AbstractAlertService, useClass: AlertService},
    ],
    imports: [
        CommonModule,
        IonicModule,
        FormsModule,
        LoggingModule,
    ],
    entryComponents: [
        NameInputBoxComponent,
        EmailInputBoxComponent
    ],
    exports: [
        NameInputBoxComponent,
        EmailInputBoxComponent
    ]
})
export class DrawablesModule {
}
