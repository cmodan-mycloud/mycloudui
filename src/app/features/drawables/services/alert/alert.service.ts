import {Injectable} from '@angular/core';
import {AbstractAlertService} from './abstract.alert.service';
import {AlertController} from '@ionic/angular';

@Injectable({
    providedIn: 'root'
})
export class AlertService implements AbstractAlertService {

    constructor(
        private alertController: AlertController
    ) {
    }

    present(errorMessage: string): void {
        this.alertController.create({
            header: 'Error',
            animated: true,
            message: errorMessage,
            buttons: ['Ok'],
            translucent: false
        }).then(async (alert: HTMLIonAlertElement) => {
            await alert.present();
        });
    }
}
