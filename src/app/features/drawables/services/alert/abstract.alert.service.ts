import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractAlertService {
    abstract present(errorMessage: string): void;

}
