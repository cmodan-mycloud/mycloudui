import {Injectable, OnDestroy} from '@angular/core';
import {AbstractLoadingService} from './abstract.loading.service';
import {Observable, Subject} from 'rxjs';
import {LoadingController} from '@ionic/angular';
import {takeUntil} from 'rxjs/operators';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';

@Injectable({
    providedIn: 'root'
})
export class LoadingService implements AbstractLoadingService, OnDestroy {
    private destroyed$ = new Subject<void>();

    constructor(
        private loadingController: LoadingController,
        private logger: AbstractLoggerService,
        private idService: AbstractIdService
    ) {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    showLoadingUntilCompleted(operation: Observable<any>, customMessage: string = 'Please wait...'): void {
        const overlayId = this.idService.generate();
        this.loadingController.create({
            id: overlayId,
            message: customMessage,
            translucent: true,
            cssClass: '',
        }).then(
            async (loading: HTMLIonLoadingElement) => {
                await loading.present();
                operation
                    .pipe(takeUntil(this.destroyed$))
                    .subscribe(
                        () => {
                        },
                        async (error) => {
                            this.logger.logDebug('Observer completed with error, closing loader: ' + error);
                            await this.loadingController.dismiss(null, null, overlayId);
                        },
                        async () => {
                            this.logger.logDebug('Observer completed successfully, closing loader.');
                            await this.loadingController.dismiss(null, null, overlayId);
                        }
                    );
            });
    }
}
