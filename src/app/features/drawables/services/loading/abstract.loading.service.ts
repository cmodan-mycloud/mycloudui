import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractLoadingService {
  abstract showLoadingUntilCompleted(operation: Observable<any>, customMessage?: string): void;
}
