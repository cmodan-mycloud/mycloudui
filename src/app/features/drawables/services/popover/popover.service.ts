import {Injectable} from '@angular/core';
import {AbstractPopoverService} from './abstract.popover.service';
import {PopoverController} from '@ionic/angular';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';

@Injectable({
    providedIn: 'root'
})
export class PopoverService implements AbstractPopoverService {
    constructor(
        private popoverController: PopoverController,
        private logger: AbstractLoggerService,
        private idService: AbstractIdService
    ) {
    }

    dismiss(id: string): void {
        this.popoverController.dismiss(id).then(() => this.logger.logDebug(`Successfully disposed popover ${id}.`));
    }

    presentWithCallback(componentRef, $event, props, callback): string {
        const popoverId = this.idService.generate();
        props.popoverId = popoverId;
        this.popoverController.create({
            id: popoverId,
            component: componentRef,
            event: $event,
            translucent: true,
            showBackdrop: true,
            componentProps: props
        }).then(async (popover) => {
            popover.onDidDismiss().then(callback);
            await popover.present();
            this.logger.logDebug(`Successfully created popover ${popoverId}.`);
        });

        return popoverId;
    }

    present(componentRef, $event, props): string {
        const popoverId = this.idService.generate();
        props.popoverId = popoverId;
        this.popoverController.create({
            id: popoverId,
            component: componentRef,
            translucent: true,
            showBackdrop: true,
            componentProps: props
        }).then(async (popover) => {
            await popover.present();
            this.logger.logDebug(`Successfully created popover ${popoverId}.`);
        });

        return popoverId;
    }
}
