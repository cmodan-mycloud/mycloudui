import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractPopoverService {

    abstract present(componentRef, $event, props): string;

    abstract dismiss(id: string): void;
}
