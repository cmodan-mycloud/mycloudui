import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
    selector: 'app-input-box',
    templateUrl: './email-input-box.component.html',
    styleUrls: ['./email-input-box.component.css']
})
export class EmailInputBoxComponent implements OnInit {
    @Input() id: string;
    public email: string;

    constructor(
        public popoverController: PopoverController,
    ) {
    }

    ngOnInit() {
    }

    async onConfirm(): Promise<void> {
        await this.popoverController.dismiss(this.email, null, this.id);
    }

    async onCancel(): Promise<void> {
        await this.popoverController.dismiss(null, null, this.id);
    }
}
