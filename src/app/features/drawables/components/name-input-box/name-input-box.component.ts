import {Component, Input, OnInit} from '@angular/core';
import {PopoverController} from '@ionic/angular';

@Component({
    selector: 'app-name-input-box',
    templateUrl: './name-input-box.component.html',
    styleUrls: ['./name-input-box.component.scss'],
})
export class NameInputBoxComponent implements OnInit {

    @Input() id: string;
    public text: string;

    constructor(
        public popoverController: PopoverController,
    ) {
    }

    ngOnInit() {
    }

    async onConfirm(): Promise<void> {
        await this.popoverController.dismiss(this.text, null, this.id);
    }

    async onCancel(): Promise<void> {
        await this.popoverController.dismiss(undefined, null, this.text);
    }

}
