import {Injectable} from '@angular/core';
import {MultipleFilesUploadRequest} from './models/multiple-files-upload-request';
import {PhotoUploadRequest} from './models/photo-upload-request';
import {UploadRequest} from '../upload/models/result/upload-request';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractUploadManagerService {
    abstract uploadToFolder(request: MultipleFilesUploadRequest): void;

    abstract uploadToRoot(request: PhotoUploadRequest): void;
}
