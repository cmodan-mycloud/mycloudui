export interface MultipleFilesUploadRequest {
    fileList: FileList;
    folderId: string;
}
