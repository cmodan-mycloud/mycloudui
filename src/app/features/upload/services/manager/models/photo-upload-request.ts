export interface PhotoUploadRequest {
    photo: Blob;
}
