import {Injectable} from '@angular/core';
import {AbstractUploadManagerService} from './abstract-upload-manager.service';
import {AbstractFileUploadService} from '../upload/abstract.file.upload.service';
import {MultipleFilesUploadRequest} from './models/multiple-files-upload-request';
import {PhotoUploadRequest} from './models/photo-upload-request';
import {HttpEventType} from '@angular/common/http';
import {filter, take} from 'rxjs/operators';
import {AbstractNotificationService} from '../../../notifications/services/notification/abstract-notification.service';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {AbstractProgressService} from '../../../progress/services/progress/abstract-progress.service';
import {TransactionModel} from '../../../progress/services/progress/models/transaction-model';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';

@Injectable({
    providedIn: 'root'
})
export class UploadManagerService implements AbstractUploadManagerService {

    constructor(
        private uploadService: AbstractFileUploadService,
        private progressService: AbstractProgressService,
        private notificationsService: AbstractNotificationService,
        private idService: AbstractIdService,
        private logger: AbstractLoggerService,
    ) {
    }

    uploadToFolder(request: MultipleFilesUploadRequest): void {
        Array.from(request.fileList).forEach(file => {
            const request$ = this.uploadService.upload({file, folderId: request.folderId});
            const transaction = {eventSource: request$, tag: `Uploading ${file.name}.`};
            this.progressService.publish(transaction);
            this.begin(transaction);
        });
    }

    uploadToRoot(request: PhotoUploadRequest): void {
        // create a name for the photo
        const id = this.idService.generate();
        const name = `photo-${id}.png`;
        // upload the photo
        const request$ = this.uploadService.uploadPhotoToRoot({file: request.photo});
        const transaction = {eventSource: request$, tag: `Uploading ${name}.`};
        this.progressService.publish(transaction);
        this.begin(transaction);
    }

    private begin(transaction: TransactionModel) {
        transaction.eventSource
            .pipe(
                filter(events => events.type === HttpEventType.Response),
                take(1)
            ).subscribe(
            () => {
                this.notificationsService.fireSuccessfulNotification(`File successfully uploaded.`, 'Upload success');
            },
            error => {
                this.notificationsService.fireFailureNotification(error.mesage, null);
                this.logger.logError(`[FAIL] ${transaction.tag} failed.`);
                this.notificationsService.fireFailureNotification(error.message, 'Error');
            },
            () => {
                this.progressService.complete(transaction);
                this.logger.logError(`${transaction.tag} completed successfully.`);
            }
        );
    }
}
