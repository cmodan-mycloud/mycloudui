import {Injectable} from '@angular/core';
import {AbstractFileUploadService} from './abstract.file.upload.service';
import {Observable, throwError} from 'rxjs';
import {HttpClient, HttpEvent} from '@angular/common/http';
import {FileUploadRequest} from './models/request/file-upload-request';
import {catchError, retry, shareReplay, tap} from 'rxjs/operators';
import {FileToRootUploadRequest} from './models/request/file-to-root-upload-request';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {environment} from '../../../../../environments/environment';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';

@Injectable({
    providedIn: 'root'
})
export class FileUploadService implements AbstractFileUploadService {

    constructor(
        private httpClient: HttpClient,
        private idService: AbstractIdService,
        private logger: AbstractLoggerService
    ) {
    }

    upload(requestModel: FileUploadRequest): Observable<HttpEvent<any>> {
        const formData = new FormData();
        formData.append('FolderId', requestModel.folderId);
        formData.append('File', requestModel.file);

        return this.httpClient
            .post(`${environment.uploadUrl}/file`, formData, {reportProgress: true, observe: 'events'})
            .pipe(
                catchError(err => throwError(err)),
                retry(environment.http.retries),
                tap(() => this.logger.logDebug(`Started uploading file under directory ${requestModel.folderId}`)),
                shareReplay(1),
            );
    }

    uploadPhotoToRoot(request: FileToRootUploadRequest): Observable<HttpEvent<any>> {
        const formData = new FormData();
        const id = this.idService.generate();
        const name = `camera-${id}.png`;
        formData.append('File', request.file, name);

        return this.httpClient
            .post(`${environment.uploadUrl}/file`, formData, {reportProgress: true, observe: 'events'})
            .pipe(
                catchError(err => throwError(err)),
                retry(environment.http.retries),
                tap(() => this.logger.logDebug(`Upload started photo ${name}`)),
                shareReplay(1),
            );
    }
}
