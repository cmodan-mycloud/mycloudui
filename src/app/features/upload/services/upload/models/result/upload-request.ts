import {Observable} from 'rxjs';
import {HttpEvent} from '@angular/common/http';

export interface UploadRequest {
    request: Observable<HttpEvent<any>>;
    tag: string;
}
