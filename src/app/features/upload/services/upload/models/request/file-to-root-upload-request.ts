export interface FileToRootUploadRequest {
    file: File | Blob;
}
