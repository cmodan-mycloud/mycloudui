export interface FileUploadRequest {
    file: File|Blob;
    folderId: string;
}
