import {Injectable} from '@angular/core';
import {FileUploadRequest} from './models/request/file-upload-request';
import {Observable} from 'rxjs';
import {HttpEvent} from '@angular/common/http';
import {FileToRootUploadRequest} from './models/request/file-to-root-upload-request';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractFileUploadService {
    abstract upload(request: FileUploadRequest): Observable<HttpEvent<any>> ;

    abstract uploadPhotoToRoot(request: FileToRootUploadRequest): Observable<HttpEvent<any>> ;
}
