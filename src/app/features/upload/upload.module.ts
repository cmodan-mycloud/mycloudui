import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AbstractFileUploadService} from './services/upload/abstract.file.upload.service';
import {FileUploadService} from './services/upload/file.upload.service';
import {AbstractUploadManagerService} from './services/manager/abstract-upload-manager.service';
import {UploadManagerService} from './services/manager/upload-manager.service';
import {ProgressModule} from '../progress/progress.module';
import {LoggingModule} from '../logging/logging.module';


@NgModule({
    declarations: [],
    providers: [
        {provide: AbstractFileUploadService, useClass: FileUploadService},
        {provide: AbstractUploadManagerService, useClass: UploadManagerService},
    ],
    imports: [
        ProgressModule,
        CommonModule,
        LoggingModule,
    ]
})
export class UploadModule {
}
