import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {PopoverController} from '@ionic/angular';
import {takeUntil} from 'rxjs/operators';
import {TransactionModel} from '../../../progress/services/progress/models/transaction-model';
import {AbstractProgressService} from '../../../progress/services/progress/abstract-progress.service';

@Component({
    selector: 'app-notification-list',
    templateUrl: './notification-list.component.html',
    styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();
    public transactions: TransactionModel[] = [];
    @Input() id: string;

    constructor(
        private progressService: AbstractProgressService,
        private popoverController: PopoverController
    ) {
    }

    ngOnInit() {
        this.progressService.transactions$
            .pipe(takeUntil(this.destroyed$))
            .subscribe(transactions => {
                this.transactions = transactions;
            });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async dismiss() {
        await this.popoverController.dismiss(null, null, this.id);
    }
}
