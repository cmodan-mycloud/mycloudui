import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {PopoverController} from '@ionic/angular';
import {NotificationListComponent} from '../notification-list/notification-list.component';
import {AbstractProgressService} from '../../../progress/services/progress/abstract-progress.service';

@Component({
    selector: 'app-notification-tray',
    templateUrl: './notification-tray.component.html',
    styleUrls: ['./notification-tray.component.scss'],
})
export class NotificationTrayComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    constructor(
        private progressService: AbstractProgressService,
        private idService: AbstractIdService,
        private popoverController: PopoverController
    ) {
    }

    ngOnInit() {
        this.progressService.transactions$
            .pipe(takeUntil(this.destroyed$))
            .subscribe();
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }


    onInteract($event: MouseEvent): void {
        const popoverId = this.idService.generate();
        this.popoverController.create({
            id: popoverId,
            component: NotificationListComponent,
            translucent: true,
            backdropDismiss: true,
            event: $event,
            componentProps: {
                id: popoverId
            }
        }).then(async popover => {
            await popover.present();
        });
    }
}
