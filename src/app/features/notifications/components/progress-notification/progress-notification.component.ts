import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {HttpEventType, HttpUploadProgressEvent} from '@angular/common/http';
import {TransactionModel} from '../../../progress/services/progress/models/transaction-model';

@Component({
    selector: 'app-progress-notification',
    templateUrl: './progress-notification.component.html',
    styleUrls: ['./progress-notification.component.scss'],
})
export class ProgressNotificationComponent implements OnInit, OnDestroy {
    @Input() transaction: TransactionModel;
    public progress: number;

    private destroyed$ = new Subject();

    constructor() {
    }

    ngOnInit() {
        this.transaction
            .eventSource
            .pipe(
                takeUntil(this.destroyed$),
                filter(notification =>
                    notification.type === HttpEventType.UploadProgress ||
                    notification.type === HttpEventType.DownloadProgress
                )
            )
            .subscribe((n) => {
                    this.progress = Math.round(100 * (n as HttpUploadProgressEvent).loaded / (n as HttpUploadProgressEvent).total) / 100;
                }
            );
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

}
