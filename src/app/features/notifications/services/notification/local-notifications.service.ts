import {Injectable} from '@angular/core';
import {AbstractNotificationService} from './abstract-notification.service';
import {Platform} from '@ionic/angular';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {ToastrService} from 'ngx-toastr';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class LocalNotificationsService implements AbstractNotificationService {

    constructor(
        private platform: Platform,
        private localNotifications: LocalNotifications,
        private toastService: ToastrService
    ) {
    }

    fireSuccessfulNotification(message: string, title: string): void {
        if (!this.isMobile()) {
            this.toastService.success(message, title, {
                timeOut: environment.toast.timeoutMs,
                positionClass: environment.toast.positionClass
            });
            return;
        }

        this.sendMobileNotification(message);
    }

    fireFailureNotification(message: string, title: string): void {
        if (!this.isMobile()) {
            this.toastService.error(message, title, {
                timeOut: environment.toast.timeoutMs,
                positionClass: environment.toast.positionClass
            });
            return;
        }
        this.sendMobileNotification(message);
    }

    private isMobile(): boolean {
        const platforms = this.platform.platforms();
        return platforms.find(p => p === 'android' || p === 'ios') !== undefined;
    }

    private sendMobileNotification(message: string): void {
        this.localNotifications.schedule({
            text: message,
        });
    }
}
