import {Injectable} from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractNotificationService {
    abstract fireSuccessfulNotification(message: string, title: string): void;
    abstract fireFailureNotification(message: string, title: string): void;
}
