import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {NotificationListComponent} from './components/notification-list/notification-list.component';
import {NotificationTrayComponent} from './components/notification-tray/notification-tray.component';
import {ProgressNotificationComponent} from './components/progress-notification/progress-notification.component';
import {IonicModule} from '@ionic/angular';
import {AbstractNotificationService} from './services/notification/abstract-notification.service';
import {LocalNotificationsService} from './services/notification/local-notifications.service';
import {LocalNotifications} from '@ionic-native/local-notifications/ngx';
import {AbstractProgressService} from '../progress/services/progress/abstract-progress.service';
import {ProgressService} from '../progress/services/progress/progress.service';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
    declarations: [
        NotificationListComponent,
        NotificationTrayComponent,
        ProgressNotificationComponent
    ],
    providers: [
        LocalNotifications,
        {provide: AbstractProgressService, useClass: ProgressService},
        {provide: AbstractNotificationService, useClass: LocalNotificationsService},
    ],
    entryComponents: [
        NotificationTrayComponent,
        NotificationListComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
    ],
    exports: [
        NotificationListComponent,
        NotificationTrayComponent,
        ProgressNotificationComponent
    ]
})
export class NotificationsModule {
}
