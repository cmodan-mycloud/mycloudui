import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {FileComponent} from './components/file/file.component';
import {FolderComponent} from './components/folder/folder.component';
import {ItemListComponent} from './components/item-list/item-list.component';
import {MenuButtonComponent} from './components/menu/menu-button/menu-button.component';
import {PopoverMenuComponent} from './components/menu/popover-menu/popover-menu.component';
import {AbstractFileService} from './services/file/abstract.file.service';
import {AbstractFolderService} from './services/folder/abstract-folder.service';
import {FileService} from './services/file/file.service';
import {FolderService} from './services/folder/folder.service';
import {BackButtonComponent} from './components/back-button/back-button.component';
import {SharedModule} from '../shared/shared.module';
import {ItemsState} from './store/items.state';
import {NgxsModule} from '@ngxs/store';
import {DrawablesModule} from '../drawables/drawables.module';
import {UploadModule} from '../upload/upload.module';
import {AbstractAvatarService} from './services/avatar/abstract-avatar.service';
import {AvatarService} from './services/avatar/avatar.service';
import {LoggingModule} from '../logging/logging.module';
import {DownloadModule} from '../download/download.module';


@NgModule({
    declarations: [
        FileComponent,
        FolderComponent,
        ItemListComponent,
        MenuButtonComponent,
        PopoverMenuComponent,
        BackButtonComponent
    ],
    entryComponents: [
        FileComponent,
        FolderComponent,
        ItemListComponent,
        MenuButtonComponent,
        PopoverMenuComponent,
        BackButtonComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        SharedModule,
        DrawablesModule,
        LoggingModule,
        UploadModule,
        DownloadModule,
        DrawablesModule,
        NgxsModule.forFeature([ItemsState])
    ],
    exports: [
        FileComponent,
        FolderComponent,
        ItemListComponent,
        MenuButtonComponent,
        PopoverMenuComponent,
        BackButtonComponent
    ],
    providers: [
        {provide: AbstractFileService, useClass: FileService},
        {provide: AbstractFolderService, useClass: FolderService},
        {provide: AbstractAvatarService, useClass: AvatarService},
    ]
})
export class ItemsModule {
}
