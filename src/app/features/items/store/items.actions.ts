import {File} from '../models/file.model';

export class InitializeWithRootItems {
    static readonly type = '[ITEMS] Get ROOT items';
}

export class NavigateToFolder {
    static readonly type = '[ITEMS] Navigate to folder';

    constructor(public id: string) {
    }
}

export class CreateFolder {
    static readonly type = '[ITEMS] Create folder';

    constructor(public name: string) {
    }
}

export class DeleteFolder {
    static readonly type = '[ITEMS] Delete folder';

    constructor(public id: string) {
    }
}

export class ShareFolder {
    static readonly type = '[ITEMS] Share folder';

    constructor(public id: string, public email: string) {
    }
}

export class RenameFolder {
    static readonly type = '[ITEMS] Rename folder';

    constructor(public id: string, public name: string) {
    }
}

export class AddFile {
    static readonly type = '[ITEMS] Add uploaded file';

    constructor(public file: File) {
    }
}

export class DeleteFile {
    static readonly type = '[ITEMS] Delete file';

    constructor(public fileId: string) {
    }
}

export class RenameFile {
    static readonly type = '[ITEMS] Rename file';

    constructor(public fileId: string, public name: string) {
    }
}

export class ShareFile {
    static readonly type = '[ITEMS] Share file';

    constructor(public fileId: string, public email: string) {
    }
}


export class ResetItems {
    static readonly type = '[ITEMS] Reset';
}
