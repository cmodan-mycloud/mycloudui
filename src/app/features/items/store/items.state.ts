import {Injectable} from '@angular/core';
import {Action, State, StateContext} from '@ngxs/store';
import {FolderItems} from '../models/folderItems';
import {
    AddFile,
    CreateFolder,
    DeleteFile,
    DeleteFolder,
    InitializeWithRootItems,
    NavigateToFolder,
    RenameFile,
    RenameFolder,
    ResetItems
} from './items.actions';
import {AbstractFolderService} from '../services/folder/abstract-folder.service';
import {filter, tap} from 'rxjs/operators';
import {FolderCreatedModel} from '../services/folder/models/response/folder.created.model';
import {AbstractFileService} from '../services/file/abstract.file.service';
import {DeletePhoto, RenamePhoto} from '../../photos/store/photos.actions';

@State<FolderItems>({
    name: 'items',
    defaults: {
        currentFolder: undefined,
        files: [],
        folders: []
    }
})
@Injectable()
export class ItemsState {

    constructor(
        private folderService: AbstractFolderService,
        private filesService: AbstractFileService
    ) {
    }

    @Action(CreateFolder)
    createFolder(ctx: StateContext<FolderItems>, action: CreateFolder) {
        const state = ctx.getState();
        return this.folderService.create(action.name, state.currentFolder.id)
            .pipe(
                filter(result => result !== null),
                tap((result: FolderCreatedModel) => {
                    state.folders.push({id: result.folderId, parentId: state.currentFolder.id, name: action.name});
                    ctx.patchState(state);
                })
            );
    }

    @Action(InitializeWithRootItems)
    getRootItems(ctx: StateContext<FolderItems>) {
        return this.folderService.getRootFolderItems()
            .pipe(
                tap((result: FolderItems) => {
                    ctx.patchState(result);
                })
            );
    }

    @Action(NavigateToFolder)
    navigateToFolder(ctx: StateContext<FolderItems>, action: NavigateToFolder) {
        return this.folderService.getFolderItems(action.id)
            .pipe(
                tap((result: FolderItems) => {
                    ctx.patchState(result);
                })
            );
    }

    @Action(RenameFolder)
    renameFolder(ctx: StateContext<FolderItems>, action: RenameFolder) {
        return this.folderService.update(action.name, action.id)
            .pipe(
                filter(isSuccessful => isSuccessful === true),
                tap(() => {
                    const state = ctx.getState();
                    const folder = state.folders.find(f => f.id === action.id);
                    const index = state.folders.indexOf(folder);
                    state.folders[index].name = action.name;
                    ctx.patchState(state);
                })
            );
    }

    @Action(DeleteFolder)
    deleteFolder(ctx: StateContext<FolderItems>, action: DeleteFolder) {
        return this.folderService.delete(action.id)
            .pipe(
                filter(isSuccessful => isSuccessful === true),
                tap(() => {
                    const state = ctx.getState();
                    state.folders = state.folders.filter(folder => {
                        return folder.id !== action.id;
                    });
                    ctx.patchState(state);
                })
            );
    }


    @Action(AddFile)
    addFile(ctx: StateContext<FolderItems>, action: AddFile) {
        const state = ctx.getState();
        if (action.file.folder.id === state.currentFolder.id) {
            state.files.push(action.file);
            ctx.patchState(state);
        }
    }

    @Action([DeleteFile, DeletePhoto])
    deleteFile(ctx: StateContext<FolderItems>, action: DeleteFile) {
        return this.filesService.remove(action.fileId)
            .pipe(
                filter(status => status === true),
                tap(() => {
                    const state = ctx.getState();
                    state.files = state.files.filter(f => f.id !== action.fileId);
                    ctx.patchState(state);
                })
            );
    }

    @Action([RenameFile, RenamePhoto])
    renameFile(ctx: StateContext<FolderItems>, action: RenameFile) {
        return this.filesService.update(action.fileId, action.name)
            .pipe(
                filter(status => status === true),
                tap(() => {
                    const state = ctx.getState();
                    const file = state.files.find(f => f.id === action.fileId);
                    const index = state.files.indexOf(file);
                    state.files[index].name = action.name;
                    ctx.patchState(state);
                })
            );
    }

    @Action([ResetItems])
    resetItems(ctx: StateContext<FolderItems>) {
        ctx.setState({
            currentFolder: undefined,
            files: [],
            folders: []
        });
    }
}
