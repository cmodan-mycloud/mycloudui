export interface AvatarModel {
    avatar: string;
    extensions: string[];
}
