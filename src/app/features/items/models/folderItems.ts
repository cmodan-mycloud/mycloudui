import {File} from './file.model';
import {Folder} from './folder.model';

export interface FolderItems {
    currentFolder: Folder;
    files: File[];
    folders: Folder[];
}
