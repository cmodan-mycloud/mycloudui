import {Folder} from './folder.model';

export interface File {
  id: string;
  name: string;
  sizeBytes: number;
  extension: string;
  folder: Folder;
  createdOn: Date;
}
