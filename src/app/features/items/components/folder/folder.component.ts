import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {PopoverController} from '@ionic/angular';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {Folder} from '../../models/folder.model';
import {Store} from '@ngxs/store';
import {DeleteFolder, NavigateToFolder, RenameFolder} from '../../store/items.actions';
import {NameInputBoxComponent} from '../../../drawables/components/name-input-box/name-input-box.component';
import {AbstractNotificationService} from '../../../notifications/services/notification/abstract-notification.service';


@Component({
    selector: 'app-folder-item',
    templateUrl: './folder.component.html',
    styleUrls: ['./folder.component.scss'],
})
export class FolderComponent implements OnInit, OnDestroy {
    @Input() folder: Folder;
    private readonly destroyed$ = new Subject<void>();

    constructor(
        public popoverController: PopoverController,
        private idService: AbstractIdService,
        private store: Store,
        private notificationsService: AbstractNotificationService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    delete() {
        this.store.dispatch(new DeleteFolder(this.folder.id))
            .subscribe(
                () => {
                    this.notificationsService.fireSuccessfulNotification(`Successfully deleted ${this.folder.name}.`, 'Success');
                },
                error => this.notificationsService.fireSuccessfulNotification(error.message, 'Error')
            );
    }

    async rename(): Promise<void> {
        const id = this.idService.generate();
        const popover = await this.popoverController.create({
            id,
            component: NameInputBoxComponent,
            translucent: true,
            showBackdrop: true,
            componentProps: {
                id
            }
        });
        popover.onDidDismiss().then(this.renameCallback.bind(this));
        await popover.present();
    }

    private renameCallback(callback: any): void {
        if (callback.data !== undefined) {
            this.store.dispatch(new RenameFolder(this.folder.id, callback.data))
                .subscribe(
                    () => {
                        const message = `Successfully renamed folder to ${this.folder.name}.`;
                        this.notificationsService.fireSuccessfulNotification(message, 'Success');
                    },
                    error => this.notificationsService.fireSuccessfulNotification(error.message, 'Error')
                );
        }
    }

    open() {
        this.store.dispatch(new NavigateToFolder(this.folder.id));
    }
}
