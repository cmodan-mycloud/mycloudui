import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Observable, Subject} from 'rxjs';
import {PopoverController} from '@ionic/angular';
import {Select, Store} from '@ngxs/store';
import {NameInputBoxComponent} from '../../../../drawables/components/name-input-box/name-input-box.component';
import {CreateFolder} from '../../../store/items.actions';
import {AbstractUploadManagerService} from '../../../../upload/services/manager/abstract-upload-manager.service';
import {Folder} from '../../../models/folder.model';
import {filter, takeUntil} from 'rxjs/operators';
import {AbstractNotificationService} from '../../../../notifications/services/notification/abstract-notification.service';

@Component({
    selector: 'app-popover-menu',
    templateUrl: './popover-menu.component.html',
    styleUrls: ['./popover-menu.component.scss'],
})
export class PopoverMenuComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    private folderId: string;
    @Input() popoverId: string;
    @Select(state => state.items.currentFolder) folder: Observable<Folder>;

    constructor(
        private popoverController: PopoverController,
        private store: Store,
        private uploadManagerService: AbstractUploadManagerService,
        private notificationsService: AbstractNotificationService
    ) {
    }

    ngOnInit() {
        this.folder
            .pipe(
                takeUntil(this.destroyed$),
                filter(folder => folder !== undefined)
            ).subscribe(folder => this.folderId = folder.id);
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async addFolder(): Promise<void> {
        await this.popoverController.dismiss(null, null, this.popoverId);
        const popover = await this.popoverController.create({
            id: this.popoverId,
            component: NameInputBoxComponent,
            componentProps: {
                id: this.popoverId
            },
            translucent: true,
            backdropDismiss: true
        });

        popover.onDidDismiss().then(this.addFolderCallback.bind(this));
        await popover.present();
    }

    private addFolderCallback(callback: any) {
        if (callback.data !== undefined) {
            this.store.dispatch(new CreateFolder(callback.data))
                .subscribe(
                    () => {
                        this.notificationsService.fireSuccessfulNotification(`Successfully created folder ${callback.data}`, 'Success');
                    },
                    error => this.notificationsService.fireSuccessfulNotification(error.message, 'Error')
                );
        }
    }

    async selectAndUpload(files: FileList): Promise<void> {
        await this.popoverController.dismiss(null, null, this.popoverId);
        if (files.length > 0) {
            this.uploadManagerService.uploadToFolder({folderId: this.folderId, fileList: files});
        }
    }
}
