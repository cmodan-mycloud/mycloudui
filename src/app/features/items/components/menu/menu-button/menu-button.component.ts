import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {PopoverController} from '@ionic/angular';
import {AbstractIdService} from '../../../../../services/id/abstract.id.service';
import {PopoverMenuComponent} from '../popover-menu/popover-menu.component';


@Component({
    selector: 'app-upload-button',
    templateUrl: './menu-button.component.html',
    styleUrls: ['./menu-button.component.scss'],
})
export class MenuButtonComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject();

    constructor(
        private popoverController: PopoverController,
        private idService: AbstractIdService
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async showMenu($event: Event) {
        const popoverId = this.idService.generate();
        const popover = await this.popoverController.create({
            id: popoverId,
            component: PopoverMenuComponent,
            event: $event,
            componentProps: {
                id: popoverId
            }
        });

        await popover.present();
    }
}
