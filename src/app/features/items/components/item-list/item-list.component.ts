import {Component, OnInit} from '@angular/core';
import {Select} from '@ngxs/store';
import {Observable} from 'rxjs';
import {ItemsState} from '../../store/items.state';
import {FolderItems} from '../../models/folderItems';

@Component({
    selector: 'app-item-list',
    templateUrl: './item-list.component.html',
    styleUrls: ['./item-list.component.scss'],
})
export class ItemListComponent implements OnInit {

    @Select(ItemsState) items: Observable<FolderItems>;

    constructor() {
    }

    ngOnInit() {
    }

}
