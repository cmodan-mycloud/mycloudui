import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {File} from '../../models/file.model';
import {AbstractAvatarService} from '../../services/avatar/abstract-avatar.service';
import {BehaviorSubject, Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {Store} from '@ngxs/store';
import {DeleteFile, RenameFile} from '../../store/items.actions';
import {PopoverController} from '@ionic/angular';
import {AbstractIdService} from '../../../../services/id/abstract.id.service';
import {NameInputBoxComponent} from '../../../drawables/components/name-input-box/name-input-box.component';
import {AbstractNotificationService} from '../../../notifications/services/notification/abstract-notification.service';
import {AbstractDownloadManagerService} from '../../../download/services/manager/abstract-download-manager.service';
import {environment} from '../../../../../environments/environment';


@Component({
    selector: 'app-file',
    templateUrl: './file.component.html',
    styleUrls: ['./file.component.scss'],
})
export class FileComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();

    @Input() file: File;
    public icon: string;
    public thumbnail: BehaviorSubject<string>;

    constructor(
        private avatarService: AbstractAvatarService,
        private store: Store,
        private popoverController: PopoverController,
        private idService: AbstractIdService,
        private notificationsService: AbstractNotificationService,
        private downloadManagerService: AbstractDownloadManagerService
    ) {
        this.thumbnail = new BehaviorSubject<string>('');
    }

    ngOnInit() {
        this.avatarService.getThumbnail(this.file.extension, this.file.id)
            .pipe(takeUntil(this.destroyed$))
            .subscribe(thumbnail => {
                this.thumbnail.next(thumbnail);
            });

    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    async delete() {
        this.store.dispatch(new DeleteFile(this.file.id))
            .subscribe(
                () => {
                    this.notificationsService.fireSuccessfulNotification(`Successfully deleted file ${this.file.name}`, 'Success');
                },
                error => this.notificationsService.fireSuccessfulNotification(error.message, 'Error')
            );
    }

    async download() {
        this.downloadManagerService.downloadFileLocally({fileId: this.file.id, fullName: this.file.name + this.file.extension});
    }

    async rename(): Promise<void> {
        const id = this.idService.generate();
        const popover = await this.popoverController.create({
            id,
            component: NameInputBoxComponent,
            translucent: true,
            showBackdrop: true,
            componentProps: {
                id
            }
        });
        popover.onDidDismiss().then(this.renameCallback.bind(this));
        await popover.present();
    }

    private renameCallback(callback: any): void {
        if (callback.data !== undefined) {
            this.store.dispatch(new RenameFile(this.file.id, callback.data))
                .subscribe(
                    () => {
                        this.notificationsService.fireSuccessfulNotification(`Successfully renamed file to ${this.file.name}`, 'Success');
                    },
                    error => this.notificationsService.fireSuccessfulNotification(error.message, 'Error')
                );
        }
    }

    public wrappedName() {
        if (this.file.name.length > environment.wrapping.text) {
            return this.file.name.substring(0, environment.wrapping.text) + '..' + this.file.extension;
        }
        return this.file.name + this.file.extension;
    }
}
