import {Component, OnDestroy, OnInit} from '@angular/core';
import {Select, Store} from '@ngxs/store';
import {Observable, Subject} from 'rxjs';
import {Folder} from '../../models/folder.model';
import {filter, takeUntil} from 'rxjs/operators';
import {NavigateToFolder} from '../../store/items.actions';

@Component({
    selector: 'app-back-button',
    templateUrl: './back-button.component.html',
    styleUrls: ['./back-button.component.scss'],
})
export class BackButtonComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    private parentId: string;
    public isRoot: boolean;

    @Select(state => state.items.currentFolder) currentFolder: Observable<Folder>;

    constructor(
        private store: Store
    ) {
    }

    ngOnInit() {
        this.isRoot = true;
        this.currentFolder
            .pipe(
                filter(folder => folder !== undefined),
                takeUntil(this.destroyed$)
            )
            .subscribe(folder => {
                this.parentId = folder.parentId;
                this.isRoot = folder.id === folder.parentId;
            });

    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    onClick() {
        this.store.dispatch(new NavigateToFolder(this.parentId));
    }
}
