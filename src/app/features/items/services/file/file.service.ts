import {Injectable} from '@angular/core';
import {AbstractFileService} from './abstract.file.service';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {catchError, map, retry, shareReplay, timeout} from 'rxjs/operators';
import {environment} from '../../../../../environments/environment';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractLoadingService} from '../../../drawables/services/loading/abstract.loading.service';
import {AbstractAlertService} from '../../../drawables/services/alert/abstract.alert.service';


@Injectable({
    providedIn: 'root'
})
export class FileService implements AbstractFileService {

    constructor(
        private httpClient: HttpClient,
        private logger: AbstractLoggerService,
        private loadingService: AbstractLoadingService,
        private alertService: AbstractAlertService
    ) {
    }

    update(fileId: string, name: string): Observable<boolean> {
        const formData = new FormData();
        formData.append('FileId', fileId);
        formData.append('Name', name);

        const request$ = this.httpClient
            .patch(`${environment.metaDataUrl}/file`, formData, {
                observe: 'response'
            })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(err => {
                    this.handleError(err);
                    return of(false);
                }),
                map((result: HttpResponse<any>) => result.status === 200),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$, `We are working on it..`);
        return request$;
    }

    remove(fileId: string): Observable<boolean> {
        const request$ = this.httpClient
            .delete(`${environment.metaDataUrl}/file`, {
                params: {
                    FileId: fileId
                },
                observe: 'response'
            })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(err => {
                    this.handleError(err);
                    return of(false);
                }),
                map((result: HttpResponse<any>) => result.status === 200),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$, `We are working on it..`);
        return request$;
    }

    private handleError(error: HttpErrorResponse) {
        this.logger.logHttpError(error);

        if (error.status === 400) {
            this.alertService.present('An error occurred when trying to rename your file.');
        }
    }
}
