import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable()
export abstract class AbstractFileService {
    abstract remove(fileId: string): Observable<boolean>;
    abstract update(fileId: string, name: string): Observable<boolean>;
}
