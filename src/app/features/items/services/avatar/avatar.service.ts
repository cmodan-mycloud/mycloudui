import {Injectable} from '@angular/core';
import {AbstractAvatarService} from './abstract-avatar.service';
import {environment} from '../../../../../environments/environment';
import {AbstractDownloadManagerService} from '../../../download/services/manager/abstract-download-manager.service';
import {Observable, of} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class AvatarService implements AbstractAvatarService {
    constructor(
        private downloadManager: AbstractDownloadManagerService,
    ) {
    }

    getThumbnail(extension: string, fileId: string): Observable<string> {
        if (this.isPhoto(extension)) {
            return this.downloadManager.downloadPhotoAsString(fileId);
        }

        const entry = environment.thumbnails.find(a => a.extensions.includes(extension));
        return entry !== undefined ? of(entry.avatar) : of('assets/icons/other.svg');
    }

    private isPhoto(extension: string): boolean {
        return environment.photos.find(a => a === extension) !== undefined;
    }
}
