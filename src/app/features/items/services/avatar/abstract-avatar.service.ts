import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractAvatarService {
    abstract getThumbnail(extension: string, fileId: string): Observable<string>;
}
