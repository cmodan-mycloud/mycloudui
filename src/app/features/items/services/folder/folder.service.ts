import {Injectable} from '@angular/core';
import {AbstractFolderService} from './abstract-folder.service';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {Observable, of, throwError} from 'rxjs';
import {FolderItems} from 'src/app/features/items/models/folderItems';
import {catchError, map, retry, shareReplay, tap, timeout} from 'rxjs/operators';
import {FolderCreatedModel} from './models/response/folder.created.model';
import {environment} from '../../../../../environments/environment';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractLoadingService} from '../../../drawables/services/loading/abstract.loading.service';
import {AbstractAlertService} from '../../../drawables/services/alert/abstract.alert.service';

@Injectable({
    providedIn: 'root'
})
export class FolderService implements AbstractFolderService {
    constructor(
        private httpClient: HttpClient,
        private logger: AbstractLoggerService,
        private alertService: AbstractAlertService,
        private loadingService: AbstractLoadingService
    ) {
    }

    getRootFolderItems(): Observable<FolderItems> {
        const request$ = this.httpClient
            .get<FolderItems>(`${environment.metaDataUrl}/folder/root`)
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(error => {
                    this.handleError(error);
                    return throwError(error);
                }),
                tap((response: FolderItems) => {
                    this.logger.logDebug(`Successfully retrieved ${response.files.length} files and ${response.folders.length} folders.`);
                }),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$, 'We are retrieving your data, hold on...');
        return request$;
    }

    getFolderItems(id: string): Observable<FolderItems> {
        const request$ = this.httpClient
            .get<FolderItems>(`${environment.metaDataUrl}/folder/items`, {
                params: {
                    FolderId: id
                }
            })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(error => {
                    this.handleError(error);
                    return of(null);
                }),
                tap(items => {
                    this.logger.logDebug(`Retrieved ${items.files.length} files and ${items.folders.length} folders for folder ${id}.`);
                }),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$, 'We are retrieving your data, hold on...');
        return request$;
    }

    update(name: string, id: string): Observable<boolean> {
        const formData = new FormData();
        formData.append('FolderId', id);
        formData.append('Name', name);

        const request$ = this.httpClient
            .patch(`${environment.metaDataUrl}/folder/update`, formData, {observe: 'response'} )
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(error => {
                    this.handleError(error);
                    return of(false);
                }),
                tap(() => {
                    this.logger.logDebug(`Successfully updated folder name to ${name}.`);
                }),
                map((response: HttpResponse<any>) => response.status === 202),
                shareReplay(1),
            );


        this.loadingService.showLoadingUntilCompleted(request$, `We trying to update your folder name, hold on..`);
        return request$;
    }

    delete(id: string): Observable<boolean> {
        const request$ = this.httpClient
            .delete(`${environment.metaDataUrl}/folder/delete`, {
                params: {
                    FolderId: id
                },
                observe: 'response'
            })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(error => {
                    this.handleError(error);
                    return of(false);
                }),
                tap(() => {
                    this.logger.logDebug(`Successfully deleted folder ${id}.`);
                }),
                    map((response: HttpResponse<any>) => response.status === 202),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$, `We trying to delete ${name}, hold on..`);
        return request$;
    }

    create(name: string, parentId: string): Observable<FolderCreatedModel> {
        const formData = new FormData();
        formData.append('Name', name);
        formData.append('ParentId', parentId);

        const request$ = this.httpClient
            .post<FolderCreatedModel>(`${environment.metaDataUrl}/folder/create`, formData)
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(error => {
                    this.handleError(error);
                    return of(null);
                }),
                tap((response: FolderCreatedModel) => {
                    this.logger.logDebug(`Successfully created folder ${name}. Given id is: ${response.folderId}.`);
                }),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$, `We trying to create ${name}, hold on..`);
        return request$;
    }

    private handleError(error: HttpErrorResponse): void {
        this.logger.logHttpError(error);

        if (error.status === 400) {
            this.alertService.present('Invalid data!');
        }
    }
}
