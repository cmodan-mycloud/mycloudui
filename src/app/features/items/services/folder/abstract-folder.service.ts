import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import { FolderItems } from 'src/app/features/items/models/folderItems';
import {FolderCreatedModel} from './models/response/folder.created.model';

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractFolderService {
  abstract getRootFolderItems(): Observable<FolderItems>;
  abstract getFolderItems(id: string): Observable<FolderItems>;
  abstract update(name: string, id: string): Observable<boolean>;
  abstract delete(id: string): Observable<boolean>;
  abstract create(name: string, parentId: string): Observable<FolderCreatedModel>;
}
