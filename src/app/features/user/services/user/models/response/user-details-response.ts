export interface UserDetailsResponse {
    name: string;
    family_name: string;
    email: string;
    sub: string;
}
