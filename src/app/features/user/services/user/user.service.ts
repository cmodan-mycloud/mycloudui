import {Injectable} from '@angular/core';
import {AbstractUserService} from './abstract.user.service';
import {Observable, of} from 'rxjs';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {catchError, map, retry, shareReplay, tap, timeout} from 'rxjs/operators';
import {UserDetailsResponse} from './models/response/user-details-response';
import {User} from '../../models/user.model';
import {environment} from '../../../../../environments/environment';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractLoadingService} from '../../../drawables/services/loading/abstract.loading.service';
import {AbstractAlertService} from '../../../drawables/services/alert/abstract.alert.service';

@Injectable({
    providedIn: 'root'
})
export class UserService implements AbstractUserService {
    constructor(
        private httpClient: HttpClient,
        private logger: AbstractLoggerService,
        private alertService: AbstractAlertService,
        private loadingService: AbstractLoadingService
    ) {
    }

    details(): Observable<User> {
        const request$ = this.httpClient
            .get<UserDetailsResponse>(`${environment.auth.authority}/connect/userinfo`, {})
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(this.handleError.bind(this)),
                tap((data: UserDetailsResponse) => {
                    this.logger.logDebug(`Successfully retrieved user details for ${data.name}.`);
                }),
                map((response: UserDetailsResponse) => {
                    return {
                        id: response.sub,
                        firstName: response.name,
                        lastName: response.family_name,
                        email: response.email
                    };
                }),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$);
        return request$;
    }

    private handleError(error: HttpErrorResponse): Observable<User> {
        this.logger.logHttpError(error);

        if (error.status === 400) {
            this.alertService.present('An error occurred.');
        }

        return of(null);
    }
}
