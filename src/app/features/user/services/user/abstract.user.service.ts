import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {User} from '../../models/user.model';

@Injectable()
export abstract class AbstractUserService {
    abstract details(): Observable<User>;
}
