import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {catchError, map, mapTo, retry, shareReplay, tap, timeout} from 'rxjs/operators';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Router} from '@angular/router';
import {AbstractAuthenticationService} from './abstract-authentication.service';
import {TokenResponseModel} from './models/token.response.model';
import {TokenRefreshModel} from './models/token.refresh.model';
import {AbstractLoggerService} from '../../../logging/services/log/abstract-logger.service';
import {AbstractAlertService} from '../../../drawables/services/alert/abstract.alert.service';
import {AbstractLoadingService} from '../../../drawables/services/loading/abstract.loading.service';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class AuthenticationService implements AbstractAuthenticationService {
    private readonly JWT_TOKEN = 'JWT_TOKEN';
    private readonly REFRESH_TOKEN = 'REFRESH_TOKEN';

    constructor(
        private http: HttpClient,
        private router: Router,
        private logger: AbstractLoggerService,
        private alertService: AbstractAlertService,
        private loadingService: AbstractLoadingService
    ) {
    }

    login(providedUsername: string, providedPassword: string): Observable<boolean> {
        const headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded'});
        const body = new HttpParams()
            .set('username', providedUsername)
            .set('password', providedPassword)
            .set('grant_type', environment.auth.grantType)
            .set('scope', environment.auth.scope)
            .set('client_id', environment.auth.clientId)
            .set('client_secret', environment.auth.clientSecret);

        const request$ = this.http
            .post<TokenResponseModel>(`${environment.auth.authority}/connect/token`, body.toString(),{
                headers
            })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(this.handleError.bind(this)),
                tap((data: TokenResponseModel) => {
                    this.storeTokens(data.access_token, data.refresh_token);
                }),
                map(result => !!result),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request$);
        return request$;
    }

    logout(): Observable<boolean> {
        const token = localStorage.getItem(this.JWT_TOKEN);
        const refreshToken = localStorage.getItem(this.REFRESH_TOKEN);
        const formData = new FormData();
        formData.append('token', refreshToken);
        formData.append('client_id', environment.auth.clientId);
        formData.append('client_secret', environment.auth.clientSecret);

        const request = this.http.post<TokenResponseModel>(`${environment.auth.authority}/connect/revocation`, formData, {
            headers: {
                Authorization: `Bearer ${token}`
            }
        })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(this.handleError.bind(this)),
                tap(
                    () => {
                        this.removeTokens();
                        this.router.navigate(['login']).then(() => {
                            this.logger.logInfo('User logged out');
                        });
                    }
                ),
                map(result => !!result),
                shareReplay(1),
            );

        this.loadingService.showLoadingUntilCompleted(request);
        return request;
    }

    isLoggedIn(): boolean {
        return !!localStorage.getItem(this.JWT_TOKEN);
    }

    getAccessToken(): string {
        return localStorage.getItem(this.JWT_TOKEN);
    }

    clear(): void {
        this.removeTokens();
    }

    refresh(): Observable<boolean> {
        const formData = new FormData();
        const token = localStorage.getItem(this.JWT_TOKEN);
        const refreshToken = localStorage.getItem(this.REFRESH_TOKEN);
        formData.append('client_id', environment.auth.clientId);
        formData.append('client_secret', environment.auth.clientSecret);
        formData.append('grant_type', environment.auth.grantType);
        formData.append('scope', environment.auth.grantType);
        formData.append('refresh_token', refreshToken);


        const request = this.http
            .post<TokenRefreshModel>(`${environment.auth.authority}/connect/token`, formData, {
                headers: {
                    Authorization: `Bearer ${token}`
                }
            })
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(this.handleError.bind(this)),
                tap(
                    (data: TokenRefreshModel) => {
                        this.storeTokens(data.access_token, data.refresh_token);
                    }
                ),
                mapTo(true),
            );

        this.loadingService.showLoadingUntilCompleted(request);
        return request;
    }

    private storeTokens(accessToken: string, refreshToken: string): void {
        localStorage.setItem(this.JWT_TOKEN, accessToken);
        localStorage.setItem(this.REFRESH_TOKEN, refreshToken);
    }

    private removeTokens(): void {
        localStorage.removeItem(this.JWT_TOKEN);
        localStorage.removeItem(this.REFRESH_TOKEN);
    }

    private handleError(error: any): Observable<boolean> {
        this.logger.logHttpError(error);
        if (error.status === 400) {
            this.alertService.present('Invalid credentials!');
        }

        return of(false);
    }
}
