import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractAuthenticationService {

    abstract login(username: string, password: string): Observable<boolean>;

    abstract logout(): Observable<boolean>;

    abstract clear(): void;

    abstract isLoggedIn(): boolean;

    abstract getAccessToken(): string;

    abstract refresh(): Observable<boolean>;
}
