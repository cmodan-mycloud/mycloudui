import {Injectable} from '@angular/core';
import {AbstractUserRegisterService} from './abstract.user.register.service';
import {UserRegisterModel} from './models/user.register.model';
import {Observable, throwError} from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {catchError, map, retry, shareReplay, timeout} from 'rxjs/operators';
import {TokenResponseModel} from '../auth/models/token.response.model';
import {environment} from '../../../../../environments/environment';

@Injectable({
    providedIn: 'root'
})
export class UserRegisterService implements AbstractUserRegisterService {

    constructor(
        private httpClient: HttpClient
    ) {
    }

    register(model: UserRegisterModel): Observable<boolean> {
        const formData = new FormData();
        formData.append('firstName', model.firstName);
        formData.append('lastName', model.lastName);
        formData.append('email', model.email);
        formData.append('password', model.password);
        return this.httpClient
            .post<TokenResponseModel>(`${environment.registrationUrl}/account/register`, formData, {observe: 'response'})
            .pipe(
                timeout(environment.http.timeoutMs),
                retry(environment.http.retries),
                catchError(error => {
                    return throwError(error.error);
                }),
                map(result => !!result),
                shareReplay(1),
            );
    }
}
