import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {UserRegisterModel} from './models/user.register.model';

@Injectable({
    providedIn: 'root'
})
export abstract class AbstractUserRegisterService {
    abstract register(model: UserRegisterModel): Observable<boolean>;
}
