import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IonicModule} from '@ionic/angular';
import {AbstractUserService} from './services/user/abstract.user.service';
import {UserService} from './services/user/user.service';
import {UserDetailsComponent} from './components/user-details/user-details.component';
import {LogoutButtonComponent} from './components/logout-button/logout-button.component';
import {DrawablesModule} from '../drawables/drawables.module';
import {LoggingModule} from '../logging/logging.module';
import {AbstractAuthenticationService} from './services/auth/abstract-authentication.service';
import {AuthenticationService} from './services/auth/authentication.service';
import {NgxsModule} from '@ngxs/store';
import {AuthState} from './state/auth.state';


@NgModule({
    declarations: [
        UserDetailsComponent,
        LogoutButtonComponent
    ],
    entryComponents: [
        UserDetailsComponent,
        LogoutButtonComponent
    ],
    imports: [
        CommonModule,
        IonicModule,
        DrawablesModule,
        LoggingModule,
        NgxsModule.forFeature([AuthState])
    ],
    providers: [
        {provide: AbstractUserService, useClass: UserService},
        {provide: AbstractAuthenticationService, useClass: AuthenticationService},
    ],
    exports: [
        UserDetailsComponent,
        LogoutButtonComponent
    ]
})
export class UserModule {
}
