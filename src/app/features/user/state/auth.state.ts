import {Action, State, StateContext} from '@ngxs/store';
import {Injectable} from '@angular/core';
import {Login, Logout} from './auth.actions';
import {AbstractAuthenticationService} from '../services/auth/abstract-authentication.service';
import {filter, tap} from 'rxjs/operators';

@State<boolean>({
    name: 'auth',
    defaults: false
})
@Injectable()
export class AuthState {

    constructor(
        private authenticationService: AbstractAuthenticationService,
    ) {
    }

    @Action(Login)
    login(ctx: StateContext<boolean>, action: Login) {
        return this.authenticationService
            .login(action.username, action.password)
            .pipe(
                filter(result => result === true),
                tap(() => ctx.setState(true))
            );
    }

    @Action(Logout)
    logout(ctx: StateContext<boolean>) {
        return this.authenticationService
            .logout()
            .pipe(
                filter(result => result === true),
                tap(() => ctx.setState(false))
            );
    }
}
