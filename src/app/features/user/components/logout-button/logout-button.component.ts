import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {Store} from '@ngxs/store';
import {Logout} from '../../state/auth.actions';

@Component({
    selector: 'app-logout-button',
    templateUrl: './logout-button.component.html',
    styleUrls: ['./logout-button.component.scss'],
})
export class LogoutButtonComponent implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();

    constructor(
        private store: Store
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    logout() {
        this.store.dispatch(new Logout());
    }
}
