import {Component, OnDestroy, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {Subject} from 'rxjs';
import {AbstractUserService} from '../../services/user/abstract.user.service';
import {filter, takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-user-details',
    templateUrl: './user-details.component.html',
    styleUrls: ['./user-details.component.scss'],
})
export class UserDetailsComponent implements OnInit, OnDestroy {
    public user: User = null;
    private destroyed$: Subject<void> = new Subject<void>();

    constructor(
        private userService: AbstractUserService,
    ) {
    }

    ngOnInit() {
        this.userService
            .details()
            .pipe(
                takeUntil(this.destroyed$),
                filter(data => data !== null)
            )
            .subscribe((user) => {
                this.user = user;
            });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
