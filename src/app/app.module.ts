import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {AppRoutingModule} from './app-routing.module';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {JwtInterceptor} from './interceptors/jwt-interceptor';

import {AbstractNetworkService} from './services/network/abstract.network.service';
import {NetworkService} from './services/network/network.service';
import {Network} from '@ionic-native/network/ngx';
import {AbstractIdService} from './services/id/abstract.id.service';
import {IdService} from './services/id/id.service';
import {SharedModule} from './features/shared/shared.module';
import {AbstractPlatformService} from './services/platform/abstract-platform.service';
import {PlatformService} from './services/platform/platform.service';
import {Actions, NgxsModule, ofActionSuccessful, Store} from '@ngxs/store';
import {NgxsReduxDevtoolsPluginModule} from '@ngxs/devtools-plugin';
import {LoggingModule} from './features/logging/logging.module';
import {DrawablesModule} from './features/drawables/drawables.module';
import {Connect, Disconnect} from './features/real-time/state/hub.actions';
import {RealTimeModule} from './features/real-time/real-time.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {UserModule} from './features/user/user.module';
import {Login, Logout} from './features/user/state/auth.actions';

@NgModule({
    declarations: [
        AppComponent,
    ],
    entryComponents: [],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        AppRoutingModule,
        HttpClientModule,
        SharedModule,
        LoggingModule,
        DrawablesModule,
        RealTimeModule,
        NgxsModule.forRoot([]),
        NgxsReduxDevtoolsPluginModule.forRoot(),
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        UserModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
        Network,
        HttpClient,
        {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
        // network
        {provide: AbstractNetworkService, useClass: NetworkService},
        // utilities
        {provide: AbstractIdService, useClass: IdService},
        {provide: AbstractPlatformService, useClass: PlatformService},
        // interceptors
        {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true},

    ],
    exports: [],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(
        private actions: Actions,
        private store: Store
    ) {
        this.actions.pipe(ofActionSuccessful(Login)).subscribe(() => this.store.dispatch(new Connect()));
        this.actions.pipe(ofActionSuccessful(Logout)).subscribe(() => this.store.dispatch(new Disconnect()));

    }
}
