export interface ErrorModel {
    errors: {
        code: string;
        description: string;
    }[];
}
