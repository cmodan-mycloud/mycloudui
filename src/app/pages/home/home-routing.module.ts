import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {HomePage} from './home.page';

const routes: Routes = [
  {
    path: '',
    component: HomePage,
    children: [
      {
        path: 'root',
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                loadChildren: () =>
                    import('../root/root.module').then(m => m.RootPageModule)
              }
            ]
          }
        ]
      },
      {
        path: 'photos',
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                loadChildren: () =>
                    import('../photos/photos.module').then(
                        m => m.PhotosPageModule
                    )
              }
            ]
          }
        ]
      },
      {
        path: 'me',
        children: [
          {
            path: '',
            children: [
              {
                path: '',
                loadChildren: () =>
                    import('../me/me.module').then(m => m.MePageModule)
              }
            ]
          }
        ]
      },
      {
        path: '',
        redirectTo: 'root',
        pathMatch: 'full'
      },
      {
        path: '*',
        redirectTo: 'root',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {
}
