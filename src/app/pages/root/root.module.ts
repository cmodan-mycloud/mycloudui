import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RootPageRoutingModule} from './files-routing.module';
import {RootPage} from './root.page';
import {SharingModule} from '../../features/sharing/sharing.module';
import {NotificationsModule} from '../../features/notifications/notifications.module';
import {ItemsModule} from '../../features/items/items.module';


@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RootPageRoutingModule,
        SharingModule,
        NotificationsModule,
        ItemsModule,
    ],
    declarations: [
        RootPage,
    ],
    entryComponents: [],
    exports: [],
    providers: []
})
export class RootPageModule {
}
