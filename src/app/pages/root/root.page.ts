import {Component, OnDestroy, OnInit} from '@angular/core';
import {Actions, ofActionSuccessful, Store} from '@ngxs/store';
import {AddFile, InitializeWithRootItems, ResetItems} from '../../features/items/store/items.actions';
import {AbstractRealtimeService} from '../../features/real-time/services/abstract-realtime.service';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {AbstractNotificationService} from '../../features/notifications/services/notification/abstract-notification.service';
import {Login, Logout} from '../../features/user/state/auth.actions';

@Component({
    selector: 'app-files',
    templateUrl: './root.page.html',
    styleUrls: ['./root.page.scss'],
})
export class RootPage implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();

    constructor(
        private store: Store,
        private communicationService: AbstractRealtimeService,
        private notificationsService: AbstractNotificationService,
        private actions: Actions
    ) {
    }

    ngOnInit() {
        this.store.dispatch(new InitializeWithRootItems());
        this.communicationService.data$
            .pipe(
                takeUntil(this.destroyed$),
                filter(data => data !== undefined),
            ).subscribe(
            file => {
                this.store.dispatch(new AddFile(file));
                this.notificationsService.fireSuccessfulNotification(`The file ${file.name} is now available.`, 'Processing completed');
            },
        );

        this.actions.pipe(ofActionSuccessful(Login)).subscribe(() => {
            this.store.dispatch(new InitializeWithRootItems());
        });

        this.actions.pipe(ofActionSuccessful(Logout)).subscribe(() => {
            this.store.dispatch(new ResetItems());
        });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
