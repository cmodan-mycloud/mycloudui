import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {RegisterPageRoutingModule} from './register-routing.module';

import {RegisterPage} from './register.page';
import {UserModule} from '../../features/user/user.module';
import {DrawablesModule} from '../../features/drawables/drawables.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        RegisterPageRoutingModule,
        ReactiveFormsModule,
        UserModule,
        DrawablesModule
    ],
  declarations: [RegisterPage]
})
export class RegisterPageModule {
}
