import {Component, OnDestroy, OnInit} from '@angular/core';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {LoadingController} from '@ionic/angular';
import {AbstractAlertService} from '../../features/drawables/services/alert/abstract.alert.service';
import {AbstractUserRegisterService} from '../../features/user/services/register/abstract.user.register.service';
import {UserRegisterService} from '../../features/user/services/register/user.register.service';
import {Router} from '@angular/router';
import {ErrorModel} from '../../errors/error.model';
import {AbstractNotificationService} from '../../features/notifications/services/notification/abstract-notification.service';

@Component({
    selector: 'app-register',
    templateUrl: './register.page.html',
    styleUrls: ['./register.page.scss'],
    providers: [
        {provide: AbstractUserRegisterService, useClass: UserRegisterService}
    ]
})
export class RegisterPage implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();
    public registerForm: FormGroup;

    constructor(
        private formBuilder: FormBuilder,
        private registerService: AbstractUserRegisterService,
        private loadingController: LoadingController,
        private alertService: AbstractAlertService,
        private router: Router,
        private localNotification: AbstractNotificationService
    ) {
    }

    ngOnInit() {
        this.registerForm = this.formBuilder.group(
            {
                firstName: ['', Validators.required],
                lastName: ['', Validators.required],
                email: ['', [Validators.required, Validators.email]],
                password: ['', Validators.required],
                confirm: ['', Validators.required]
            },
            {
                validator: this.MustMatch('password', 'confirm')
            }
        );
    }

    onSignUp(): void {
        if (this.registerForm.invalid) {
            this.alertService.present('Invalid data provided!');
            return;
        }

        this.loadingController.create({
            message: 'Please wait...',
            translucent: false
        }).then(async (loading: HTMLIonLoadingElement) => {
            await loading.present();
            this.registerService
                .register(
                    {
                        firstName: this.registerForm.controls.firstName.value,
                        lastName: this.registerForm.controls.lastName.value,
                        email: this.registerForm.controls.email.value,
                        password: this.registerForm.controls.password.value
                    }
                )
                .pipe(takeUntil(this.destroyed$))
                .subscribe(
                    async () => {
                        await this.loadingController.dismiss();
                        await this.router.navigate(['/login']);
                        this.localNotification.fireSuccessfulNotification('Registration succeeded!', 'Success');
                    },
                    async (error: ErrorModel) => {
                        await error.errors.forEach(errorData => {
                            this.loadingController.dismiss();
                            this.localNotification.fireFailureNotification(errorData.description, 'Failure');
                        });
                    }
                );
        });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

    MustMatch(controlName: string, matchingControlName: string) {
        return (formGroup: FormGroup) => {
            const control = formGroup.controls[controlName];
            const matchingControl = formGroup.controls[matchingControlName];

            if (matchingControl.errors && !matchingControl.errors.mustMatch) {
                // return if another validator has already found an error on the matchingControl
                return;
            }

            // set error on matchingControl if validation fails
            if (control.value !== matchingControl.value) {
                matchingControl.setErrors({mustMatch: true});
            } else {
                matchingControl.setErrors(null);
            }
        };
    }

}
