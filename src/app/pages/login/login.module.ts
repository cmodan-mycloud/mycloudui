import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {LoginPageRoutingModule} from './login-routing.module';

import {LoginPage} from './login.page';
import {DrawablesModule} from '../../features/drawables/drawables.module';
import {UserModule} from '../../features/user/user.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        LoginPageRoutingModule,
        ReactiveFormsModule,
        DrawablesModule,
        UserModule
    ],
    declarations: [LoginPage],
    providers: []
})
export class LoginPageModule {
}
