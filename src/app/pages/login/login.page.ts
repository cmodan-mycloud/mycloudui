import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Subject} from 'rxjs';
import {AbstractAlertService} from '../../features/drawables/services/alert/abstract.alert.service';
import {Store} from '@ngxs/store';
import {Login} from '../../features/user/state/auth.actions';

@Component({
    selector: 'app-login',
    templateUrl: './login.page.html',
    styleUrls: ['./login.page.scss'],
    providers: [FormBuilder]
})
export class LoginPage implements OnInit, OnDestroy {
    public loginForm: FormGroup;
    private destroyed$ = new Subject();

    constructor(
        private formBuilder: FormBuilder,
        private router: Router,
        private alertService: AbstractAlertService,
        private store: Store
    ) {
    }

    get f() {
        return this.loginForm.controls;
    }

    ngOnInit() {
        this.loginForm = this.formBuilder.group({
            username: ['', [Validators.required, Validators.email]],
            password: ['', Validators.required]
        });
    }

    onSignIn(): void {
        if (this.loginForm.invalid) {
            this.alertService.present('Please provide a valid email address and password!');
            return;
        }

        this.store.dispatch(new Login(this.f.username.value, this.f.password.value)).subscribe(
            async () => await this.router.navigate(['/root'])
        );
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
