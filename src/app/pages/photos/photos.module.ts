import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {PhotosPageRoutingModule} from './photos-routing.module';

import {PhotosPage} from './photos.page';
import {NotificationsModule} from '../../features/notifications/notifications.module';
import {PhotosModule} from '../../features/photos/photos.module';
import {DownloadModule} from '../../features/download/download.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        DownloadModule,
        PhotosPageRoutingModule,
        NotificationsModule,
        PhotosModule,
    ],
    declarations: [
        PhotosPage,
    ],
    entryComponents: []
})
export class PhotosPageModule {
}
