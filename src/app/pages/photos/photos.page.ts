import {Component, OnDestroy, OnInit} from '@angular/core';
import {Actions, ofActionSuccessful, Store} from '@ngxs/store';
import {AddPhoto, GetPhotos, ResetPhotos} from '../../features/photos/store/photos.actions';
import {AbstractRealtimeService} from '../../features/real-time/services/abstract-realtime.service';
import {Subject} from 'rxjs';
import {filter, takeUntil} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {AbstractNotificationService} from '../../features/notifications/services/notification/abstract-notification.service';
import {Login, Logout} from '../../features/user/state/auth.actions';

@Component({
    selector: 'app-photos',
    templateUrl: './photos.page.html',
    styleUrls: ['./photos.page.scss']
})
export class PhotosPage implements OnInit, OnDestroy {
    private destroyed$ = new Subject<void>();

    constructor(
        private store: Store,
        private realtimeService: AbstractRealtimeService,
        private notificationsService: AbstractNotificationService,
        private actions: Actions
    ) {
    }

    ngOnInit() {
        this.store.dispatch(new GetPhotos());
        this.realtimeService.data$.pipe(
            takeUntil(this.destroyed$),
            filter(file => file !== undefined),
            filter(file => environment.photos.some(f => f === file.extension))
        ).subscribe(photo => {
            this.store.dispatch(new AddPhoto(photo));
            this.notificationsService.fireSuccessfulNotification(`Your photo is now available.`, 'Processing completed');
        });

        this.actions.pipe(ofActionSuccessful(Login)).subscribe(() => {
            this.store.dispatch(new GetPhotos());
        });

        this.actions.pipe(ofActionSuccessful(Logout)).subscribe(() => {
            this.store.dispatch(new ResetPhotos());
        });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }
}
