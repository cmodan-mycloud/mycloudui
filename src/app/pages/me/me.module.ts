import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {MePageRoutingModule} from './me-routing.module';

import {MePage} from './me.page';
import {NotificationsModule} from '../../features/notifications/notifications.module';
import {UserModule} from '../../features/user/user.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        MePageRoutingModule,
        NotificationsModule,
        UserModule,
    ],
    declarations: [
        MePage,
    ],
    exports: [],
    entryComponents: [],
    providers: []
})
export class MePageModule {
}
