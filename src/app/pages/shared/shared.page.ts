import {Component, OnDestroy, OnInit} from '@angular/core';
import {AbstractSharingService} from '../../features/sharing/services/sharing/abstract.sharing.service';
import {ShareItem} from '../../features/sharing/models/share.model';
import {Subject} from 'rxjs';
import {takeUntil} from 'rxjs/operators';

@Component({
    selector: 'app-shared',
    templateUrl: './shared.page.html',
    styleUrls: ['./shared.page.scss'],
})
export class SharedPage implements OnInit, OnDestroy {
    public incomingShares: ShareItem[];
    public outgoingShares: ShareItem[];

    private readonly destroyed$ = new Subject<void>();

    constructor(
        private sharingService: AbstractSharingService
    ) {
    }

    ngOnInit() {
        this.sharingService
            .incomingShares()
            .pipe(takeUntil(this.destroyed$))
            .subscribe((shares) => {
                this.incomingShares = shares;
            });

        this.sharingService
            .outgoingShares()
            .pipe(takeUntil(this.destroyed$))
            .subscribe((shares) => {
                this.outgoingShares = shares;
            });
    }

    ngOnDestroy(): void {
        this.destroyed$.next();
        this.destroyed$.complete();
    }

}
