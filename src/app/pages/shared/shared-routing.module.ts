import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {SharedPage} from './shared.page';

const routes: Routes = [
  {
    path: '',
    component: SharedPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SharedPageRoutingModule {
}
