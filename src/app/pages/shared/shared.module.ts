import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {IonicModule} from '@ionic/angular';

import {SharedPageRoutingModule} from './shared-routing.module';

import {SharedPage} from './shared.page';
import {MePageModule} from '../me/me.module';
import {SharingModule} from '../../features/sharing/sharing.module';
import {NotificationsModule} from '../../features/notifications/notifications.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        IonicModule,
        SharedPageRoutingModule,
        MePageModule,
        SharingModule,
        NotificationsModule
    ],
    providers: [],
    declarations: [
        SharedPage,
    ]
})
export class SharedPageModule {
}
