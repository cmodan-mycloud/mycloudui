import {Injectable} from '@angular/core';
import {HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Router} from '@angular/router';
import {AbstractLoggerService} from '../features/logging/services/log/abstract-logger.service';
import {AbstractAlertService} from '../features/drawables/services/alert/abstract.alert.service';
import {Store} from '@ngxs/store';
import {Disconnect} from '../features/real-time/state/hub.actions';
import {AuthenticationService} from '../features/user/services/auth/authentication.service';

@Injectable({
    providedIn: 'root'
})
export class JwtInterceptor implements HttpInterceptor {

    constructor(
        private authenticationService: AuthenticationService,
        private logger: AbstractLoggerService,
        private alertService: AbstractAlertService,
        private router: Router,
        private store: Store
    ) {
    }

    private static doesRequireToken(request: HttpRequest<any>): boolean {
        return !request.url.endsWith('connect/token') ||
            !request.url.endsWith('account/register');
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (JwtInterceptor.doesRequireToken(request) && this.authenticationService.isLoggedIn()) {
            const token = this.authenticationService.getAccessToken();
            request = request.clone({
                setHeaders: {
                    Authorization: `Bearer ${token}`
                }
            });
        }

        return next.handle(request)
            .pipe(
                catchError(async (error) => {
                    if (error instanceof HttpErrorResponse && error.status === 401) {
                        this.store.dispatch(new Disconnect());
                        this.authenticationService.clear();
                        await this.router.navigate(['/login']);
                    }

                    throw error;
                })
            );
    }
}
