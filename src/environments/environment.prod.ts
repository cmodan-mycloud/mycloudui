export const environment = {
    production: true,
    uploadUrl: '',
    downloadUrl: '',
    metaDataUrl: '',
    registrationUrl: '',
    signalR: {
        endpoint: '',
        timeout: 5000
    },
    auth: {
        authority: '',
        grantType: 'password',
        scope: 'offline_access openid profile upload download management metadata',
        clientId: 'spa',
        clientSecret: 'secret'
    },
    http: {
        timeoutMs: 5000,
        retries: 0
    },
    signalREndpoint: '',
    thumbnails: [
        {
            avatar: 'assets/icons/mp3.svg',
            extensions: ['.mp3']
        },
        {
            avatar: 'assets/icons/mp4.svg',
            extensions: ['.mp4']
        },
        {
            avatar: 'assets/icons/word.svg',
            extensions: ['.doc', '.docx']
        },
        {
            avatar: 'assets/icons/ppt.svg',
            extensions: ['.ppt', '.pptx']
        },
        {
            avatar: 'assets/icons/pdf.svg',
            extensions: ['.pdf']
        },
        {
            avatar: 'assets/icons/archive.svg',
            extensions: ['.rar', '.zip', '.7z']
        }
    ],
    photos: [
        '.jpg', '.png', '.jpeg', '.svg', '.tif'
    ],
    toast: {
        timeoutMs: 4000,
        positionClass: 'toast-top-right'
    },
    wrapping: {
        text: 10
    }
};
