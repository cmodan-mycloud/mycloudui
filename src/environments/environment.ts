// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    uploadUrl: 'http://localhost:5009',
    downloadUrl: 'http://localhost:5003',
    metaDataUrl: 'http://localhost:5005',
    registrationUrl: 'https://localhost:5001',
    signalR: {
        endpoint: 'http://localhost:5011',
        timeout: 5000
    },
    auth: {
        authority: 'https://localhost:5001',
        grantType: 'password',
        scope: 'openid profile upload download metadata notifications',
        clientId: 'spa',
        clientSecret: 'secret'
    },
    http: {
        timeoutMs: 5000,
        retries: 1
    },
    thumbnails: [
        {
            avatar: 'assets/icons/mp3.svg',
            extensions: ['.mp3']
        },
        {
            avatar: 'assets/icons/mp4.svg',
            extensions: ['.mp4']
        },
        {
            avatar: 'assets/icons/word.svg',
            extensions: ['.doc', '.docx']
        },
        {
            avatar: 'assets/icons/ppt.svg',
            extensions: ['.ppt', '.pptx']
        },
        {
            avatar: 'assets/icons/pdf.svg',
            extensions: ['.pdf']
        },
        {
            avatar: 'assets/icons/archive.svg',
            extensions: ['.rar', '.zip', '.7z']
        }
    ],
    photos: [
        '.jpg', '.png', '.jpeg', '.svg', '.tif'
    ],
    toast: {
        timeoutMs: 4000,
        positionClass: 'toast-top-right'
    },
    wrapping: {
        text: 5
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
