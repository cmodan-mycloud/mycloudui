# Setup:

1. Make sure you have Node and NPM installed on your machine
2. Execute `npm install`
3. Execute `ionic build`. This will build the android project.
4. Execute `npx cap add android` to create the android folder.
5. Execute `npx cap open android` to open the project in android studio.
6. Execute `ionic capacitor run android -l --host=localhost --external` to run the application. Make sure you select the proper network adapter. The app and the server must be on the same network.
7. Build the project.
8. Click on the play button and select the device.
